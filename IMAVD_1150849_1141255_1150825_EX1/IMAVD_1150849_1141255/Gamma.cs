﻿using IMAVD_1150849_1141255_1150825;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255
{
    public partial class Gamma : Form
    {

        Bitmap preview = null;
        LoadImage original = null;
        Form1 form = null;
        Filter f = new Filter();

        public Gamma(LoadImage image, Form1 form)
        {
            InitializeComponent();
            preview = new Bitmap(image.image, pictureBox1.Width, pictureBox1.Height);
            pictureBox1.Image = preview;
            this.form = form;
            this.original = image;
            pictureBox1.Refresh();
            scrollValue.Text = "1";
        }

        private void apply_btn_Click(object sender, EventArgs e)
        {
            this.form.imageHistory.Add(this.form.image);
            this.form.index++;

            float gamma = (float)trackBar1.Value / 10f;

            this.form.image.setImage(f.AdjustGamma(original.bitmap, gamma));
            this.form.image.refreshPictureBox();
            this.form.centerPictureBox();

            this.Close();
            this.Dispose();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

            float gamma = (float)trackBar1.Value / 10f;

            scrollValue.Text = gamma.ToString();
            pictureBox1.Image = f.AdjustGamma(preview, gamma);
            pictureBox1.Refresh();
        }
    }
}
