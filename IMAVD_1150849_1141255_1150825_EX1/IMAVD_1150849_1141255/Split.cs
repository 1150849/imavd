﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAVD_1150849_1141255
{
    class Split
    {

        private readonly int space = 6;

        internal Bitmap SplitInFour(Image image)
        {
            return SplitImage(image, 4);
        }

        internal Bitmap SplitInTwo(Image image)
        {
            return SplitImage(image, 2);
        }

        internal Bitmap SplitIntoUpperCorner(Image image)
        {
            return SplitImage(image, 1);
        }

        internal Bitmap SplitIntoLowerCorner(Image image)
        {
            return SplitImage(image, -1);
        }

        private Bitmap SplitImage(Image image, int operation)
        {
            int spaceHeight = 0;
            int spaceWidth = 0;
            double proportion = (double)image.Width / image.Height;
            Bitmap bitImage = new Bitmap(image);
            int bitHeight = image.Height;

            if (operation == 2 || operation == 4) bitHeight += space;

            double bitWidth = image.Width;

            if (operation == 4) bitWidth += space;

            Bitmap bit = new Bitmap((int)bitWidth, bitHeight);

            if (operation == -1)
            {
                bitWidth = 0;
            }

            int middleWidth = image.Width / 2;
            int middleHeight = image.Height / 2;
            for (int j = 0; j < bitHeight; j++)
            {
                spaceWidth = 0;
                if (j == middleHeight && (operation == 2 || operation == 4))
                {
                    j += space;
                    spaceHeight = space;
                }
                for (int i = 0; i < bitWidth; i++)
                {
                    if (i == middleWidth && operation == 4)
                    {
                        i += space;
                        spaceWidth = space;
                    }
                    if (operation == -1)
                    {
                        bit.SetPixel(image.Width - 1 - i, j, bitImage.GetPixel(image.Width - 1-i, j));
                    }
                    else
                    {
                        bit.SetPixel(i, j, bitImage.GetPixel(i - spaceWidth, j - spaceHeight));
                    }
                }
                if (operation == 1)
                {
                    bitWidth -= proportion;
                }
                else if (operation == -1)
                {
                    bitWidth += proportion;
                }
            }
            return bit;
        }
    }
}
