﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255_1150825
{
    public class LoadImage
    {
        public PictureBox picBox;
        public Image image;
        public Bitmap bitmap;

        public string name { get; set; }
        public string type { get; set; }
        public string location { get; set; }
        public string dimension { get; set; }
        public long size { get; set; }
        public DateTime creationDate { get; set; }

        public double zoomValue { get; set; }

        public LoadImage(Image image, PictureBox picBox, FileInfo fi)
        {
            this.picBox = picBox;
            this.image = image;
            this.name = fi.Name;
            this.type = fi.Extension;
            this.location = fi.FullName;
            this.dimension = image.Size.Width + " x " + image.Size.Height;
            this.size = fi.Length;
            this.creationDate = fi.CreationTime;
            this.bitmap = (Bitmap)image;
            this.zoomValue = 1.0;

            refreshPictureBox();
        }

        public Bitmap getImage()
        {
            return this.bitmap; 
        }

        public void setImage(Bitmap image)
        {
            this.image = image;
        }

        public void refreshPictureBox()
        {
            int width = (int)((double) this.image.Width * this.zoomValue);
            int height = (int)((double) this.image.Height * this.zoomValue);

            this.picBox.SizeMode = PictureBoxSizeMode.AutoSize;
            this.picBox.Image = new Bitmap(this.image, width, height);
            this.picBox.Refresh();
        }

    }
}
