﻿using IMAVD_1150849_1141255_1150825;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255
{
    public partial class InsertText : Form
    {
        LoadImage original = null;
        Form1 form = null;

        public InsertText(LoadImage image, Form1 form)
        {
            InitializeComponent();
            this.form = form;
            this.original = image;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.form.imageHistory.Add(this.form.image);
            this.form.index++;

            Bitmap layeredImg = new Bitmap(this.original.image);

            using (Graphics g = Graphics.FromImage(layeredImg))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                g.DrawString(text_txt.Text, new Font("Microsoft Sans Serif", layeredImg.Width / 20), Brushes.Black, new Point(0, 0));
            }

            this.form.image.setImage(layeredImg);
            this.form.image.refreshPictureBox();
            this.form.centerPictureBox();

            this.Close();
            this.Dispose();
        }

    }
}