﻿using IMAVD_1150849_1141255_1150825;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255
{
    public partial class ChromakeyRemoval : Form
    {
        Form1 form = null;
        Filter f = new Filter();
        Bitmap preview = null;

        public ChromakeyRemoval(LoadImage image, Form1 form)
        {
            InitializeComponent();

            preview = removal((Bitmap)image.image);

            pictureBox1.Image = new Bitmap(preview, pictureBox1.Width, pictureBox1.Height);
            this.form = form;
            pictureBox1.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.form.imageHistory.Add(this.form.image);
            this.form.index++;

            this.form.image.setImage(preview);
            this.form.image.refreshPictureBox();
            this.form.centerPictureBox();

            this.Close();
            this.Dispose();
        }

        private Bitmap removal(Bitmap bp)
        {
            Bitmap output = new Bitmap(bp.Width, bp.Height);

            for (int y = 0; y < output.Height; y++)
            {
                for (int x = 0; x < output.Width; x++)
                {
                    Color camColor = bp.GetPixel(x, y);

                    byte max = Math.Max(Math.Max(camColor.R, camColor.G), camColor.B);
                    byte min = Math.Min(Math.Min(camColor.R, camColor.G), camColor.B);

                    bool replace =
                        camColor.G != min // green is not the smallest value
                        && (camColor.G == max // green is the biggest value
                        || max - camColor.G < 8) // or at least almost the biggest value
                        && (max - min) > 96; // minimum difference between smallest/biggest value (avoid grays)

                    if (replace)
                        camColor = Color.Transparent;

                    output.SetPixel(x, y, camColor);
                }
            }
            return output;
        }

    }
}
