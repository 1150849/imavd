﻿using IMAVD_1150849_1141255;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Windows.Forms;



namespace IMAVD_1150849_1141255_1150825
{
    public partial class Form1 : Form
    {
        public LoadImage image;
        public string path;
        Filter f = new Filter();
        public int index = 0;
        Split split = new Split();

        public List<LoadImage> imageHistory = new List<LoadImage>();

        public Form1()
        {
            InitializeComponent();
            Color defaultColor = Color.Black;
            zoomBox.SelectedIndex = 4;

            if (image == null)
            {
                zoomBox.Visible = false;
                imageToolStripMenuItem.Enabled = false;
                editToolStripMenuItem.Enabled = false;
                insertToolStripMenuItem.Enabled = false;
                undo_btn.Enabled = false;
                save_btn.Enabled = false;
            }
        }

        private void loadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog
            {
                Title = "Select Image",
                CheckFileExists = true,
                CheckPathExists = true
            };

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                path = ofd.FileName;
                Image i = Image.FromFile(path);
                FileInfo fi = new FileInfo(path);

                image = new LoadImage(i, pictureBox, fi);
                centerPictureBox();

                zoomBox.Visible = true;
                imageToolStripMenuItem.Enabled = true;
                editToolStripMenuItem.Enabled = true;
                insertToolStripMenuItem.Enabled = true;
                undo_btn.Enabled = true;
                save_btn.Enabled = true;
            }
        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (image == null)
            {
                MessageBox.Show("You have to select a picture before!");
            }
            else
            {
                ImageProperties form = new ImageProperties(image);
                form.ShowDialog();
            }
        }

        private void checkColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            // Keeps the user from selecting a custom color.
            MyDialog.AllowFullOpen = true;

            Color selectedColor = new Color();

            // Update the text box color if the user clicks OK 
            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
                if (image == null)
                {
                    MessageBox.Show("You have to select a picture before!");
                }
                else
                {
                    selectedColor = MyDialog.Color;
                    if (checkBitmapColor(selectedColor) == true)
                    {
                        MessageBox.Show("The selected color exists in the image");
                    }
                    else
                    {
                        MessageBox.Show("The selected color does not exists in the image");
                    }
                }
            }
        }

        public Boolean checkBitmapColor(Color selectedColor)
        {
            for (int y = 0; y < image.image.Height; y++)
            {
                for (int x = 0; x < image.image.Width; x++)
                {
                    if (((Bitmap)image.image).GetPixel(x, y).ToArgb() == selectedColor.ToArgb())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void zoomBox_DropDownClosed(object sender, EventArgs e)
        {
            if (image == null)
            {
                MessageBox.Show("You have to select a picture before!");
            }
            else
            {
                double zoomValue = double.Parse(zoomBox.Text.Replace("%", "")) / 100;
                image.zoomValue = zoomValue;

                image.refreshPictureBox();
                centerPictureBox();
            }
        }

        public void centerPictureBox()
        {
            foreach (var scroll in this.panel1.Controls.OfType<ScrollBar>())
            {
                if (scroll.Visible)
                    return;
            }

            Point center = new Point((panel1.Width - pictureBox.Width) / 2, (panel1.Height - pictureBox.Height) / 2);
            center.X = center.X < 0 ? 0 : center.X;
            center.Y = center.Y < 0 ? 0 : center.Y;
            pictureBox.Location = center;
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            index++;

            Bitmap bp = f.addRedFilter(image.image);
            image.setImage(bp);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            index++;

            Bitmap bp = f.addGreenFilter(image.image);
            image.setImage(bp);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            index++;

            Bitmap bp = f.addBlueFilter(image.image);
            image.setImage(bp);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void brightnessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Brightness form = new Brightness(image, this);
            form.ShowDialog();
        }

        private void contrastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Contrast form = new Contrast(image, this);
            form.ShowDialog();
        }

        private void chromaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChromakeyRemoval form = new ChromakeyRemoval(image, this);
            form.ShowDialog();
        }

        private void undo_btn_Click(object sender, EventArgs e)
        {
            image.setImage(imageHistory[index - 1].bitmap);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void resizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Resize form = new Resize(image, this);
            form.ShowDialog();
        }

        private void ºToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            index++;

            Image original = image.image;
            using (Graphics g = Graphics.FromImage(original))
            {
                // Set Middle Point to Rotate
                g.TranslateTransform(original.Width / 2, original.Height / 2);
                // Rotate
                g.RotateTransform(45);
                // Set Middle Point
                g.TranslateTransform(-original.Width / 2, -original.Height / 2);
                // Desenho
                g.DrawImage(original, new Point(0, 0));
            }
            image.setImage((Bitmap)original);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void ºToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            index++;

            Image original = image.image;
            using (Graphics g = Graphics.FromImage(original))
            {
                // Set Middle Point to Rotate
                g.TranslateTransform(original.Width / 2, original.Height / 2);
                // Rotate
                g.RotateTransform(90);
                // Set Middle Point
                g.TranslateTransform(-original.Width / 2, -original.Height / 2);
                // Desenho
                g.DrawImage(original, new Point(0, 0));
            }
            image.setImage((Bitmap)original);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void ºToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            index++;

            Image original = image.image;
            using (Graphics g = Graphics.FromImage(original))
            {
                // Set Middle Point to Rotate
                g.TranslateTransform(original.Width / 2, original.Height / 2);
                // Rotate
                g.RotateTransform(180);
                // Set Middle Point
                g.TranslateTransform(-original.Width / 2, -original.Height / 2);
                // Desenho
                g.DrawImage(original, new Point(0, 0));
            }
            image.setImage((Bitmap)original);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void verticallyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            index++;

            Image original = image.image;
            using (Graphics g = Graphics.FromImage(original))
            {
                // Set Middle Point to Rotate
                g.TranslateTransform(original.Width / 2, original.Height / 2);
                // Rotate
                g.RotateTransform(180);
                // Set Middle Point
                g.TranslateTransform(-original.Width / 2, -original.Height / 2);
                // Desenho
                g.DrawImage(original, new Point(0, 0));
            }
            image.setImage((Bitmap)original);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            index++;

            image.image.RotateFlip(RotateFlipType.RotateNoneFlipX);

            image.setImage((Bitmap)image.image);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void textToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InsertText form = new InsertText(image, this);
            form.ShowDialog();
        }

        private void imageToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Image files (*.png, *.jpg, ...) | *.png;*.jpg;*.jpeg";
            DialogResult res = fd.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                Bitmap layeredImg = new Bitmap(this.image.image);
                Bitmap img = new Bitmap(fd.FileName);

                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    g.DrawImage(img, new Rectangle(0, 0, (img.Size.Width < this.image.image.Size.Width) 
                        ? img.Size.Width : this.image.image.Size.Width, 
                        (img.Size.Height < this.image.image.Size.Height) 
                        ? img.Size.Height : this.image.image.Size.Height));
                }

                image.setImage(layeredImg);
                image.refreshPictureBox();
                centerPictureBox();
            }
        }

        private void invertColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            index++;

            Bitmap bp = f.Invert(image.image);
            image.setImage(bp);
            image.refreshPictureBox();
            centerPictureBox();
        }

        private void gammaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Gamma form = new Gamma(image, this);
            form.ShowDialog();
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            SaveFileDialog ofd = new SaveFileDialog
            {
                Title = "Save Image",
                Filter = "Bitmap Image (.bmp)|*.bmp|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png",
                FilterIndex = 3,
                FileName = "default_" + DateTime.Now.ToString("ddMMyyyy_HHmmss"),
                RestoreDirectory = true
            };

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (ofd.FileName != null)
                {
                    string path = ofd.FileName;

                    switch (ofd.FilterIndex)
                    {
                        case 1:
                            image.image.Save(path, System.Drawing.Imaging.ImageFormat.Bmp);
                            break;
                        case 2:
                            image.image.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                            break;
                        case 3:
                            image.image.Save(path, System.Drawing.Imaging.ImageFormat.Png);
                            break;
                        default:
                            image.image.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("Pick a title!");
                }
            }
        }


        private void inFourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            image.setImage(split.SplitInFour(pictureBox.Image));
            image.refreshPictureBox();
        }

        private void inTwoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            image.setImage(split.SplitInTwo(pictureBox.Image));
            image.refreshPictureBox();
        }

        private void upperCornerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            image.setImage(split.SplitIntoUpperCorner(pictureBox.Image));
            image.refreshPictureBox();
        }

        private void lowerCornerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            image.setImage(split.SplitIntoLowerCorner(pictureBox.Image));
            image.refreshPictureBox();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageHistory.Add(image);
            image.setImage(image.bitmap);
            image.refreshPictureBox();
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {

        }

        private void speech_btn_Click(object sender, EventArgs e)
        {
            IMAVD_1150849_1141255.Speech form = new IMAVD_1150849_1141255.Speech(this);
            form.ShowDialog();
        }
    }
}
