﻿namespace IMAVD_1150849_1141255_1150825
{
    partial class ImageProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.Label();
            this.type_txt = new System.Windows.Forms.Label();
            this.location_txt = new System.Windows.Forms.Label();
            this.dimension_txt = new System.Windows.Forms.Label();
            this.size_txt = new System.Windows.Forms.Label();
            this.date_txt = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Type:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Location:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Dimension:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Size:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Date:";
            // 
            // txt_name
            // 
            this.txt_name.AutoSize = true;
            this.txt_name.Location = new System.Drawing.Point(64, 25);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(0, 17);
            this.txt_name.TabIndex = 6;
            // 
            // type_txt
            // 
            this.type_txt.AutoSize = true;
            this.type_txt.Location = new System.Drawing.Point(59, 62);
            this.type_txt.Name = "type_txt";
            this.type_txt.Size = new System.Drawing.Size(0, 17);
            this.type_txt.TabIndex = 7;
            // 
            // location_txt
            // 
            this.location_txt.Location = new System.Drawing.Point(81, 99);
            this.location_txt.Name = "location_txt";
            this.location_txt.Size = new System.Drawing.Size(282, 45);
            this.location_txt.TabIndex = 8;
            // 
            // dimension_txt
            // 
            this.dimension_txt.AutoSize = true;
            this.dimension_txt.Location = new System.Drawing.Point(92, 145);
            this.dimension_txt.Name = "dimension_txt";
            this.dimension_txt.Size = new System.Drawing.Size(0, 17);
            this.dimension_txt.TabIndex = 9;
            // 
            // size_txt
            // 
            this.size_txt.AutoSize = true;
            this.size_txt.Location = new System.Drawing.Point(54, 177);
            this.size_txt.Name = "size_txt";
            this.size_txt.Size = new System.Drawing.Size(0, 17);
            this.size_txt.TabIndex = 10;
            // 
            // date_txt
            // 
            this.date_txt.AutoSize = true;
            this.date_txt.Location = new System.Drawing.Point(57, 210);
            this.date_txt.Name = "date_txt";
            this.date_txt.Size = new System.Drawing.Size(0, 17);
            this.date_txt.TabIndex = 11;
            // 
            // ImageProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 236);
            this.Controls.Add(this.date_txt);
            this.Controls.Add(this.size_txt);
            this.Controls.Add(this.dimension_txt);
            this.Controls.Add(this.location_txt);
            this.Controls.Add(this.type_txt);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ImageProperties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image Properties";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label txt_name;
        private System.Windows.Forms.Label type_txt;
        private System.Windows.Forms.Label location_txt;
        private System.Windows.Forms.Label dimension_txt;
        private System.Windows.Forms.Label size_txt;
        private System.Windows.Forms.Label date_txt;
    }
}