﻿using IMAVD_1150849_1141255_1150825;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255
{
    public partial class Contrast : Form
    {
        Bitmap bp = null;
        LoadImage original = null;
        Form1 form = null;
        Filter f = new Filter();

        public Contrast(LoadImage image, Form1 form)
        {
            InitializeComponent();
            bp = new Bitmap(image.image, pictureBox1.Width, pictureBox1.Height);
            pictureBox1.Image = bp;
            this.form = form;
            this.original = image;
            pictureBox1.Refresh();
            scrollValue.Text = trackBar1.Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.form.imageHistory.Add(this.form.image);
            this.form.index++;

            this.form.image.setImage(f.adjustContrast(original.image, trackBar1.Value));
            this.form.image.refreshPictureBox();
            this.form.centerPictureBox();

            this.Close();
            this.Dispose();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            scrollValue.Text = trackBar1.Value.ToString();

            pictureBox1.Image = f.adjustContrast(bp, trackBar1.Value);
            pictureBox1.Refresh();
        }
    }
}
