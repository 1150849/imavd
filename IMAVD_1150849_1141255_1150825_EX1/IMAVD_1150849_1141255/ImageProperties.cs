﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255_1150825
{
    public partial class ImageProperties : Form
    {
        public ImageProperties(LoadImage image)
        {
            InitializeComponent();
            this.txt_name.Text = image.name;
            this.type_txt.Text = image.type;
            this.location_txt.Text = image.location;
            this.dimension_txt.Text = image.dimension;
            this.size_txt.Text = ((double)image.size/1000).ToString() + " Kb ";
            this.date_txt.Text = image.creationDate.ToString();
        }

    }
}
