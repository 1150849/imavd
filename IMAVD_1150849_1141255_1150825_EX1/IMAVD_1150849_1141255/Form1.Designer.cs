﻿namespace IMAVD_1150849_1141255_1150825
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applyFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brightnessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contrastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chromaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gammaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fIlpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ºToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ºToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ºToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.verticallyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inFourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inTwoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upperCornerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowerCornerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomBox = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.undo_btn = new System.Windows.Forms.Button();
            this.save_btn = new System.Windows.Forms.Button();
            this.speech_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(506, 431);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.imageToolStripMenuItem,
            this.editToolStripMenuItem,
            this.insertToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(538, 25);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadImageToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(39, 21);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadImageToolStripMenuItem
            // 
            this.loadImageToolStripMenuItem.Name = "loadImageToolStripMenuItem";
            this.loadImageToolStripMenuItem.Size = new System.Drawing.Size(147, 24);
            this.loadImageToolStripMenuItem.Text = "Load Image";
            this.loadImageToolStripMenuItem.Click += new System.EventHandler(this.loadImageToolStripMenuItem_Click);
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.propertiesToolStripMenuItem,
            this.checkColorToolStripMenuItem,
            this.applyFilterToolStripMenuItem,
            this.brightnessToolStripMenuItem,
            this.contrastToolStripMenuItem,
            this.chromaToolStripMenuItem,
            this.resizeToolStripMenuItem,
            this.invertColorToolStripMenuItem,
            this.gammaToolStripMenuItem});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(56, 21);
            this.imageToolStripMenuItem.Text = "Image";
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.propertiesToolStripMenuItem.Text = "Properties";
            this.propertiesToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // checkColorToolStripMenuItem
            // 
            this.checkColorToolStripMenuItem.Name = "checkColorToolStripMenuItem";
            this.checkColorToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.checkColorToolStripMenuItem.Text = "Check Color";
            this.checkColorToolStripMenuItem.Click += new System.EventHandler(this.checkColorToolStripMenuItem_Click);
            // 
            // applyFilterToolStripMenuItem
            // 
            this.applyFilterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redToolStripMenuItem,
            this.greenToolStripMenuItem,
            this.blueToolStripMenuItem});
            this.applyFilterToolStripMenuItem.Name = "applyFilterToolStripMenuItem";
            this.applyFilterToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.applyFilterToolStripMenuItem.Text = "Apply Filter";
            // 
            // redToolStripMenuItem
            // 
            this.redToolStripMenuItem.Name = "redToolStripMenuItem";
            this.redToolStripMenuItem.Size = new System.Drawing.Size(113, 24);
            this.redToolStripMenuItem.Text = "Red";
            this.redToolStripMenuItem.Click += new System.EventHandler(this.redToolStripMenuItem_Click);
            // 
            // greenToolStripMenuItem
            // 
            this.greenToolStripMenuItem.Name = "greenToolStripMenuItem";
            this.greenToolStripMenuItem.Size = new System.Drawing.Size(113, 24);
            this.greenToolStripMenuItem.Text = "Green";
            this.greenToolStripMenuItem.Click += new System.EventHandler(this.greenToolStripMenuItem_Click);
            // 
            // blueToolStripMenuItem
            // 
            this.blueToolStripMenuItem.Name = "blueToolStripMenuItem";
            this.blueToolStripMenuItem.Size = new System.Drawing.Size(113, 24);
            this.blueToolStripMenuItem.Text = "Blue";
            this.blueToolStripMenuItem.Click += new System.EventHandler(this.blueToolStripMenuItem_Click);
            // 
            // brightnessToolStripMenuItem
            // 
            this.brightnessToolStripMenuItem.Name = "brightnessToolStripMenuItem";
            this.brightnessToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.brightnessToolStripMenuItem.Text = "Brightness";
            this.brightnessToolStripMenuItem.Click += new System.EventHandler(this.brightnessToolStripMenuItem_Click);
            // 
            // contrastToolStripMenuItem
            // 
            this.contrastToolStripMenuItem.Name = "contrastToolStripMenuItem";
            this.contrastToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.contrastToolStripMenuItem.Text = "Contrast";
            this.contrastToolStripMenuItem.Click += new System.EventHandler(this.contrastToolStripMenuItem_Click);
            // 
            // chromaToolStripMenuItem
            // 
            this.chromaToolStripMenuItem.Name = "chromaToolStripMenuItem";
            this.chromaToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.chromaToolStripMenuItem.Text = "Chromakey Removal";
            this.chromaToolStripMenuItem.Click += new System.EventHandler(this.chromaToolStripMenuItem_Click);
            // 
            // resizeToolStripMenuItem
            // 
            this.resizeToolStripMenuItem.Name = "resizeToolStripMenuItem";
            this.resizeToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.resizeToolStripMenuItem.Text = "Resize";
            this.resizeToolStripMenuItem.Click += new System.EventHandler(this.resizeToolStripMenuItem_Click);
            // 
            // invertColorToolStripMenuItem
            // 
            this.invertColorToolStripMenuItem.Name = "invertColorToolStripMenuItem";
            this.invertColorToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.invertColorToolStripMenuItem.Text = "Invert Colors";
            this.invertColorToolStripMenuItem.Click += new System.EventHandler(this.invertColorToolStripMenuItem_Click);
            // 
            // gammaToolStripMenuItem
            // 
            this.gammaToolStripMenuItem.Name = "gammaToolStripMenuItem";
            this.gammaToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.gammaToolStripMenuItem.Text = "Gamma";
            this.gammaToolStripMenuItem.Click += new System.EventHandler(this.gammaToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fIlpToolStripMenuItem,
            this.toolStripMenuItem1});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(42, 21);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // fIlpToolStripMenuItem
            // 
            this.fIlpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ºToolStripMenuItem,
            this.ºToolStripMenuItem1,
            this.ºToolStripMenuItem2,
            this.verticallyToolStripMenuItem,
            this.horizontalToolStripMenuItem});
            this.fIlpToolStripMenuItem.Name = "fIlpToolStripMenuItem";
            this.fIlpToolStripMenuItem.Size = new System.Drawing.Size(143, 24);
            this.fIlpToolStripMenuItem.Text = "Flip Image";
            // 
            // ºToolStripMenuItem
            // 
            this.ºToolStripMenuItem.Name = "ºToolStripMenuItem";
            this.ºToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.ºToolStripMenuItem.Text = "45º";
            this.ºToolStripMenuItem.Click += new System.EventHandler(this.ºToolStripMenuItem_Click);
            // 
            // ºToolStripMenuItem1
            // 
            this.ºToolStripMenuItem1.Name = "ºToolStripMenuItem1";
            this.ºToolStripMenuItem1.Size = new System.Drawing.Size(138, 24);
            this.ºToolStripMenuItem1.Text = "90º";
            this.ºToolStripMenuItem1.Click += new System.EventHandler(this.ºToolStripMenuItem1_Click);
            // 
            // ºToolStripMenuItem2
            // 
            this.ºToolStripMenuItem2.Name = "ºToolStripMenuItem2";
            this.ºToolStripMenuItem2.Size = new System.Drawing.Size(138, 24);
            this.ºToolStripMenuItem2.Text = "180º";
            this.ºToolStripMenuItem2.Click += new System.EventHandler(this.ºToolStripMenuItem2_Click);
            // 
            // verticallyToolStripMenuItem
            // 
            this.verticallyToolStripMenuItem.Name = "verticallyToolStripMenuItem";
            this.verticallyToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.verticallyToolStripMenuItem.Text = "Vertical";
            this.verticallyToolStripMenuItem.Click += new System.EventHandler(this.verticallyToolStripMenuItem_Click);
            // 
            // horizontalToolStripMenuItem
            // 
            this.horizontalToolStripMenuItem.Name = "horizontalToolStripMenuItem";
            this.horizontalToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.horizontalToolStripMenuItem.Text = "Horizontal";
            this.horizontalToolStripMenuItem.Click += new System.EventHandler(this.horizontalToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inFourToolStripMenuItem,
            this.inTwoToolStripMenuItem,
            this.upperCornerToolStripMenuItem,
            this.lowerCornerToolStripMenuItem,
            this.resetToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(143, 24);
            this.toolStripMenuItem1.Text = "Split Image";
            // 
            // inFourToolStripMenuItem
            // 
            this.inFourToolStripMenuItem.Name = "inFourToolStripMenuItem";
            this.inFourToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.inFourToolStripMenuItem.Text = "In Four";
            this.inFourToolStripMenuItem.Click += new System.EventHandler(this.inFourToolStripMenuItem_Click);
            // 
            // inTwoToolStripMenuItem
            // 
            this.inTwoToolStripMenuItem.Name = "inTwoToolStripMenuItem";
            this.inTwoToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.inTwoToolStripMenuItem.Text = "In Two";
            this.inTwoToolStripMenuItem.Click += new System.EventHandler(this.inTwoToolStripMenuItem_Click);
            // 
            // upperCornerToolStripMenuItem
            // 
            this.upperCornerToolStripMenuItem.Name = "upperCornerToolStripMenuItem";
            this.upperCornerToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.upperCornerToolStripMenuItem.Text = "Upper Corner";
            this.upperCornerToolStripMenuItem.Click += new System.EventHandler(this.upperCornerToolStripMenuItem_Click);
            // 
            // lowerCornerToolStripMenuItem
            // 
            this.lowerCornerToolStripMenuItem.Name = "lowerCornerToolStripMenuItem";
            this.lowerCornerToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.lowerCornerToolStripMenuItem.Text = "Lower Corner";
            this.lowerCornerToolStripMenuItem.Click += new System.EventHandler(this.lowerCornerToolStripMenuItem_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.resetToolStripMenuItem.Text = "Reset Split";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // insertToolStripMenuItem
            // 
            this.insertToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.textToolStripMenuItem,
            this.imageToolStripMenuItem1});
            this.insertToolStripMenuItem.Name = "insertToolStripMenuItem";
            this.insertToolStripMenuItem.Size = new System.Drawing.Size(52, 21);
            this.insertToolStripMenuItem.Text = "Insert";
            // 
            // textToolStripMenuItem
            // 
            this.textToolStripMenuItem.Name = "textToolStripMenuItem";
            this.textToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.textToolStripMenuItem.Text = "Text";
            this.textToolStripMenuItem.Click += new System.EventHandler(this.textToolStripMenuItem_Click);
            // 
            // imageToolStripMenuItem1
            // 
            this.imageToolStripMenuItem1.Name = "imageToolStripMenuItem1";
            this.imageToolStripMenuItem1.Size = new System.Drawing.Size(114, 24);
            this.imageToolStripMenuItem1.Text = "Image";
            this.imageToolStripMenuItem1.Click += new System.EventHandler(this.imageToolStripMenuItem1_Click);
            // 
            // zoomBox
            // 
            this.zoomBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.zoomBox.FormattingEnabled = true;
            this.zoomBox.Items.AddRange(new object[] {
            "500%",
            "400%",
            "300%",
            "200%",
            "100%",
            "50%"});
            this.zoomBox.Location = new System.Drawing.Point(424, 469);
            this.zoomBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.zoomBox.Name = "zoomBox";
            this.zoomBox.Size = new System.Drawing.Size(92, 21);
            this.zoomBox.TabIndex = 5;
            this.zoomBox.DropDownClosed += new System.EventHandler(this.zoomBox_DropDownClosed);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Location = new System.Drawing.Point(9, 32);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(506, 431);
            this.panel1.TabIndex = 6;
            // 
            // undo_btn
            // 
            this.undo_btn.AutoSize = true;
            this.undo_btn.Location = new System.Drawing.Point(80, 470);
            this.undo_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.undo_btn.Name = "undo_btn";
            this.undo_btn.Size = new System.Drawing.Size(56, 23);
            this.undo_btn.TabIndex = 7;
            this.undo_btn.Text = "Undo";
            this.undo_btn.UseVisualStyleBackColor = true;
            this.undo_btn.Click += new System.EventHandler(this.undo_btn_Click);
            // 
            // save_btn
            // 
            this.save_btn.AutoSize = true;
            this.save_btn.Location = new System.Drawing.Point(9, 470);
            this.save_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(56, 23);
            this.save_btn.TabIndex = 8;
            this.save_btn.Text = "Save";
            this.save_btn.UseVisualStyleBackColor = true;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // speech_btn
            // 
            this.speech_btn.Location = new System.Drawing.Point(264, 470);
            this.speech_btn.Name = "speech_btn";
            this.speech_btn.Size = new System.Drawing.Size(75, 23);
            this.speech_btn.TabIndex = 9;
            this.speech_btn.Text = "Speech";
            this.speech_btn.UseVisualStyleBackColor = true;
            this.speech_btn.Click += new System.EventHandler(this.speech_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 501);
            this.Controls.Add(this.speech_btn);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.undo_btn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.zoomBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(500, 499);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image Editor";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkColorToolStripMenuItem;
        private System.Windows.Forms.ComboBox zoomBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem applyFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brightnessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contrastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chromaToolStripMenuItem;
        private System.Windows.Forms.Button undo_btn;
        private System.Windows.Forms.ToolStripMenuItem resizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fIlpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ºToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ºToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ºToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem verticallyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem invertColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gammaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem inFourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inTwoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upperCornerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lowerCornerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.Button speech_btn;
    }
}

