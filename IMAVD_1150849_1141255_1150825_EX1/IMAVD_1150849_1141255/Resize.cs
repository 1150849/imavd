﻿using IMAVD_1150849_1141255_1150825;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255
{
    public partial class Resize : Form
    {
        LoadImage original = null;
        Form1 form = null;

        public Resize(LoadImage image, Form1 form)
        {
            InitializeComponent();
            original = image;
            this.form = form;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.form.imageHistory.Add(this.form.image);
            this.form.index++;

            this.form.image.setImage(new Bitmap(original.image, Int32.Parse(width_txt.Text), Int32.Parse(lenght_txt.Text)));
            this.form.image.refreshPictureBox();
            this.form.centerPictureBox();

            this.Close();
            this.Dispose();
        }
    }
}
