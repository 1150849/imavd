﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace IMAVD_1150849_1141255_1150825
{
    public class Shape
    {
        public Image image;
        public Bitmap bitmap;
        public int cumulativeRotation;
        public string shape;
        public int x;
        public int y;
        public Color color;
        public int scale;
        public double scaleFactor = 0.25;
        public int margin = 0;

        public Shape(Image image, string shape, int x, int y)
        {
            this.image = image;
            this.cumulativeRotation = 0;
            this.shape = shape;
            this.x = x;
            this.y = y;
            this.color = Color.Black;
            this.scale = 1;
            this.margin = 55;
        }

        public Shape(Image image, string shape, int x, int y, Color color)
        {
            this.image = image;
            this.cumulativeRotation = 0;
            this.shape = shape;
            this.x = x;
            this.y = y;
            this.color = color;
            this.scale = 1;
            this.margin = 55;
            this.changeColor(color);
        }

        public Image rotateShapeLeft()
        {
            this.cumulativeRotation += 5;
            Image original = this.image;
            Bitmap rotated = new Bitmap(original.Width, original.Height);

            using (Graphics g = Graphics.FromImage(rotated))
            {
                g.TranslateTransform(original.Width / 2, original.Height / 2);
                g.RotateTransform(cumulativeRotation);
                g.TranslateTransform(-original.Width / 2, -original.Height / 2);
                g.DrawImage(original, new Point(0, 0));
            }

            return new Bitmap(rotated, original.Width, original.Height);
        }


        public Image rotateShapeRight()
        {
            this.cumulativeRotation -= 5;
            Image original = this.image;
            Bitmap rotated = new Bitmap(original.Width, original.Height);

            using (Graphics g = Graphics.FromImage(rotated))
            {
                g.TranslateTransform(original.Width / 2, original.Height / 2);
                g.RotateTransform(cumulativeRotation);
                g.TranslateTransform(-original.Width / 2, -original.Height / 2);
                g.DrawImage(original, new Point(0, 0));
            }

            return new Bitmap(rotated, original.Width, original.Height);
        }

        public Image atualRotation()
        {
            Image original = this.image;
            Bitmap rotated = new Bitmap(original.Width, original.Height);

            using (Graphics g = Graphics.FromImage(rotated))
            {
                g.TranslateTransform(original.Width / 2, original.Height / 2);
                g.RotateTransform(cumulativeRotation);
                g.TranslateTransform(-original.Width / 2, -original.Height / 2);
                g.DrawImage(original, new Point(0, 0));

                return new Bitmap(rotated, original.Width, original.Height); ;
            }
        }

        public Image changeColor(Color color)
        {
            Image original = this.image;

            Bitmap rotated = new Bitmap(original.Width, original.Height);

            using (Graphics g = Graphics.FromImage(rotated))
            {
                g.TranslateTransform(original.Width / 2, original.Height / 2);
                g.RotateTransform(cumulativeRotation);
                g.TranslateTransform(-original.Width / 2, -original.Height / 2);
                g.DrawImage(original, new Point(0, 0));
            }

            Bitmap output = new Bitmap(original.Width, original.Height);

            for (int y = 0; y < output.Height; y++)
            {
                for (int x = 0; x < output.Width; x++)
                {
                    Color camColor = rotated.GetPixel(x, y);

                    byte max = Math.Max(Math.Max(camColor.R, camColor.G), camColor.B);
                    byte min = Math.Min(Math.Min(camColor.R, camColor.G), camColor.B);

                    if (camColor.R != 255 && camColor.G != 255 && camColor.B != 255)
                    {
                        if(camColor.A == 255)
                        {
                            output.SetPixel(x, y, color);
                        }
                        else {
                            output.SetPixel(x, y, Color.White);
                        }
                    }
                    else
                    {
                        output.SetPixel(x, y, Color.White);
                    }
                }
            }
            this.color = color;
            this.image = output;
            return output;
        }

        public Image scaleUp()
        {
            Image original = this.image;

            var brush = new SolidBrush(Color.Green);

            this.margin = this.margin + 1;

            return resizeImage(this.image, this.margin, this.margin);
        }

        public Image scaleDown()
        {
            Image original = this.image;

            var brush = new SolidBrush(Color.Green);

            this.margin = this.margin - 1;

            return resizeImage(this.image, this.margin, this.margin);
        }

        public static Bitmap resizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

    }
}
