﻿namespace IMAVD_1150849_1141255_1150825
{
    partial class Application
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.addSquare_btn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.stop_square_rotation = new System.Windows.Forms.Button();
            this.changeColor_txt = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.circleScaleDown_btn = new System.Windows.Forms.Button();
            this.triangleScaleDown_btn = new System.Windows.Forms.Button();
            this.squareScaleDown_btn = new System.Windows.Forms.Button();
            this.rotateCircleRight = new System.Windows.Forms.Button();
            this.rotateTriangleRight_btn = new System.Windows.Forms.Button();
            this.rotateSquareRight_btn = new System.Windows.Forms.Button();
            this.circleScaleUp_btn = new System.Windows.Forms.Button();
            this.changeColorCircle_btn = new System.Windows.Forms.Button();
            this.stopCircleRotation_btn = new System.Windows.Forms.Button();
            this.startCircleRotation_btn = new System.Windows.Forms.Button();
            this.addCircle_btn = new System.Windows.Forms.Button();
            this.triangleScaleUp_btn = new System.Windows.Forms.Button();
            this.changeColorTriangle_btn = new System.Windows.Forms.Button();
            this.stopRotationTriangle_btn = new System.Windows.Forms.Button();
            this.startTriangleRotation_btn = new System.Windows.Forms.Button();
            this.addTriangle_btn = new System.Windows.Forms.Button();
            this.scale_up_btn = new System.Windows.Forms.Button();
            this.help_btn = new System.Windows.Forms.Button();
            this.duplicateSquare_btn = new System.Windows.Forms.Button();
            this.duplicateTriangle_btn = new System.Windows.Forms.Button();
            this.duplicateCircle_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(13, 10);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(772, 414);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(350, 433);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 28);
            this.button1.TabIndex = 1;
            this.button1.Text = "Text Speech";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // addSquare_btn
            // 
            this.addSquare_btn.AutoSize = true;
            this.addSquare_btn.Location = new System.Drawing.Point(40, 10);
            this.addSquare_btn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addSquare_btn.Name = "addSquare_btn";
            this.addSquare_btn.Size = new System.Drawing.Size(97, 28);
            this.addSquare_btn.TabIndex = 2;
            this.addSquare_btn.Text = "Add Square";
            this.addSquare_btn.UseVisualStyleBackColor = true;
            this.addSquare_btn.Click += new System.EventHandler(this.addSquare_btn_Click);
            // 
            // button2
            // 
            this.button2.AutoSize = true;
            this.button2.Location = new System.Drawing.Point(13, 44);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(159, 28);
            this.button2.TabIndex = 3;
            this.button2.Text = "Rotate Square Left";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // stop_square_rotation
            // 
            this.stop_square_rotation.AutoSize = true;
            this.stop_square_rotation.Location = new System.Drawing.Point(13, 115);
            this.stop_square_rotation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.stop_square_rotation.Name = "stop_square_rotation";
            this.stop_square_rotation.Size = new System.Drawing.Size(159, 28);
            this.stop_square_rotation.TabIndex = 4;
            this.stop_square_rotation.Text = "Stop Square Rotation";
            this.stop_square_rotation.UseVisualStyleBackColor = true;
            this.stop_square_rotation.Click += new System.EventHandler(this.stop_square_rotation_Click);
            // 
            // changeColor_txt
            // 
            this.changeColor_txt.AutoSize = true;
            this.changeColor_txt.Location = new System.Drawing.Point(13, 149);
            this.changeColor_txt.Name = "changeColor_txt";
            this.changeColor_txt.Size = new System.Drawing.Size(159, 27);
            this.changeColor_txt.TabIndex = 5;
            this.changeColor_txt.Text = "Change Color Square";
            this.changeColor_txt.UseVisualStyleBackColor = true;
            this.changeColor_txt.Click += new System.EventHandler(this.changeColor_txt_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.duplicateCircle_btn);
            this.panel1.Controls.Add(this.duplicateTriangle_btn);
            this.panel1.Controls.Add(this.duplicateSquare_btn);
            this.panel1.Controls.Add(this.circleScaleDown_btn);
            this.panel1.Controls.Add(this.triangleScaleDown_btn);
            this.panel1.Controls.Add(this.squareScaleDown_btn);
            this.panel1.Controls.Add(this.rotateCircleRight);
            this.panel1.Controls.Add(this.rotateTriangleRight_btn);
            this.panel1.Controls.Add(this.rotateSquareRight_btn);
            this.panel1.Controls.Add(this.circleScaleUp_btn);
            this.panel1.Controls.Add(this.changeColorCircle_btn);
            this.panel1.Controls.Add(this.stopCircleRotation_btn);
            this.panel1.Controls.Add(this.startCircleRotation_btn);
            this.panel1.Controls.Add(this.addCircle_btn);
            this.panel1.Controls.Add(this.triangleScaleUp_btn);
            this.panel1.Controls.Add(this.changeColorTriangle_btn);
            this.panel1.Controls.Add(this.stopRotationTriangle_btn);
            this.panel1.Controls.Add(this.startTriangleRotation_btn);
            this.panel1.Controls.Add(this.addTriangle_btn);
            this.panel1.Controls.Add(this.scale_up_btn);
            this.panel1.Controls.Add(this.stop_square_rotation);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.changeColor_txt);
            this.panel1.Controls.Add(this.addSquare_btn);
            this.panel1.Location = new System.Drawing.Point(13, 471);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(772, 251);
            this.panel1.TabIndex = 6;
            // 
            // circleScaleDown_btn
            // 
            this.circleScaleDown_btn.AutoSize = true;
            this.circleScaleDown_btn.Location = new System.Drawing.Point(421, 215);
            this.circleScaleDown_btn.Name = "circleScaleDown_btn";
            this.circleScaleDown_btn.Size = new System.Drawing.Size(161, 27);
            this.circleScaleDown_btn.TabIndex = 22;
            this.circleScaleDown_btn.Text = "Circle Scale Down";
            this.circleScaleDown_btn.UseVisualStyleBackColor = true;
            this.circleScaleDown_btn.Click += new System.EventHandler(this.circleScaleDown_btn_Click);
            // 
            // triangleScaleDown_btn
            // 
            this.triangleScaleDown_btn.AutoSize = true;
            this.triangleScaleDown_btn.Location = new System.Drawing.Point(208, 216);
            this.triangleScaleDown_btn.Name = "triangleScaleDown_btn";
            this.triangleScaleDown_btn.Size = new System.Drawing.Size(161, 27);
            this.triangleScaleDown_btn.TabIndex = 21;
            this.triangleScaleDown_btn.Text = "Triangle Scale Down";
            this.triangleScaleDown_btn.UseVisualStyleBackColor = true;
            this.triangleScaleDown_btn.Click += new System.EventHandler(this.triangleScaleDown_btn_Click);
            // 
            // squareScaleDown_btn
            // 
            this.squareScaleDown_btn.AutoSize = true;
            this.squareScaleDown_btn.Location = new System.Drawing.Point(13, 215);
            this.squareScaleDown_btn.Name = "squareScaleDown_btn";
            this.squareScaleDown_btn.Size = new System.Drawing.Size(159, 27);
            this.squareScaleDown_btn.TabIndex = 20;
            this.squareScaleDown_btn.Text = "Square Scale Down";
            this.squareScaleDown_btn.UseVisualStyleBackColor = true;
            this.squareScaleDown_btn.Click += new System.EventHandler(this.squareScaleDown_btn_Click);
            // 
            // rotateCircleRight
            // 
            this.rotateCircleRight.AutoSize = true;
            this.rotateCircleRight.Location = new System.Drawing.Point(420, 80);
            this.rotateCircleRight.Name = "rotateCircleRight";
            this.rotateCircleRight.Size = new System.Drawing.Size(162, 27);
            this.rotateCircleRight.TabIndex = 19;
            this.rotateCircleRight.Text = "Rotate Circle Right";
            this.rotateCircleRight.UseVisualStyleBackColor = true;
            this.rotateCircleRight.Click += new System.EventHandler(this.rotateCircleRight_Click);
            // 
            // rotateTriangleRight_btn
            // 
            this.rotateTriangleRight_btn.AutoSize = true;
            this.rotateTriangleRight_btn.Location = new System.Drawing.Point(208, 80);
            this.rotateTriangleRight_btn.Name = "rotateTriangleRight_btn";
            this.rotateTriangleRight_btn.Size = new System.Drawing.Size(162, 27);
            this.rotateTriangleRight_btn.TabIndex = 18;
            this.rotateTriangleRight_btn.Text = "Rotate Triangle Right";
            this.rotateTriangleRight_btn.UseVisualStyleBackColor = true;
            this.rotateTriangleRight_btn.Click += new System.EventHandler(this.rotateTriangleRight_btn_Click);
            // 
            // rotateSquareRight_btn
            // 
            this.rotateSquareRight_btn.AutoSize = true;
            this.rotateSquareRight_btn.Location = new System.Drawing.Point(13, 80);
            this.rotateSquareRight_btn.Name = "rotateSquareRight_btn";
            this.rotateSquareRight_btn.Size = new System.Drawing.Size(159, 27);
            this.rotateSquareRight_btn.TabIndex = 17;
            this.rotateSquareRight_btn.Text = "Rotate Square Right";
            this.rotateSquareRight_btn.UseVisualStyleBackColor = true;
            this.rotateSquareRight_btn.Click += new System.EventHandler(this.rotateSquareRight_btn_Click);
            // 
            // circleScaleUp_btn
            // 
            this.circleScaleUp_btn.AutoSize = true;
            this.circleScaleUp_btn.Location = new System.Drawing.Point(429, 182);
            this.circleScaleUp_btn.Name = "circleScaleUp_btn";
            this.circleScaleUp_btn.Size = new System.Drawing.Size(143, 27);
            this.circleScaleUp_btn.TabIndex = 16;
            this.circleScaleUp_btn.Text = "Circle Scale Up";
            this.circleScaleUp_btn.UseVisualStyleBackColor = true;
            this.circleScaleUp_btn.Click += new System.EventHandler(this.circleScaleUp_btn_Click);
            // 
            // changeColorCircle_btn
            // 
            this.changeColorCircle_btn.AutoSize = true;
            this.changeColorCircle_btn.Location = new System.Drawing.Point(429, 149);
            this.changeColorCircle_btn.Name = "changeColorCircle_btn";
            this.changeColorCircle_btn.Size = new System.Drawing.Size(144, 27);
            this.changeColorCircle_btn.TabIndex = 15;
            this.changeColorCircle_btn.Text = "Change Color Circle";
            this.changeColorCircle_btn.UseVisualStyleBackColor = true;
            this.changeColorCircle_btn.Click += new System.EventHandler(this.changeColorCircle_btn_Click);
            // 
            // stopCircleRotation_btn
            // 
            this.stopCircleRotation_btn.AutoSize = true;
            this.stopCircleRotation_btn.Location = new System.Drawing.Point(429, 116);
            this.stopCircleRotation_btn.Name = "stopCircleRotation_btn";
            this.stopCircleRotation_btn.Size = new System.Drawing.Size(143, 27);
            this.stopCircleRotation_btn.TabIndex = 14;
            this.stopCircleRotation_btn.Text = "Stop Circle Rotation";
            this.stopCircleRotation_btn.UseVisualStyleBackColor = true;
            this.stopCircleRotation_btn.Click += new System.EventHandler(this.stopCircleRotation_btn_Click);
            // 
            // startCircleRotation_btn
            // 
            this.startCircleRotation_btn.AutoSize = true;
            this.startCircleRotation_btn.Location = new System.Drawing.Point(429, 43);
            this.startCircleRotation_btn.Name = "startCircleRotation_btn";
            this.startCircleRotation_btn.Size = new System.Drawing.Size(144, 27);
            this.startCircleRotation_btn.TabIndex = 13;
            this.startCircleRotation_btn.Text = "Rotate Circle Left";
            this.startCircleRotation_btn.UseVisualStyleBackColor = true;
            this.startCircleRotation_btn.Click += new System.EventHandler(this.startCircleRotation_btn_Click);
            // 
            // addCircle_btn
            // 
            this.addCircle_btn.AutoSize = true;
            this.addCircle_btn.Location = new System.Drawing.Point(461, 10);
            this.addCircle_btn.Name = "addCircle_btn";
            this.addCircle_btn.Size = new System.Drawing.Size(82, 27);
            this.addCircle_btn.TabIndex = 12;
            this.addCircle_btn.Text = "Add Circle";
            this.addCircle_btn.UseVisualStyleBackColor = true;
            this.addCircle_btn.Click += new System.EventHandler(this.addCircle_btn_Click);
            // 
            // triangleScaleUp_btn
            // 
            this.triangleScaleUp_btn.AutoSize = true;
            this.triangleScaleUp_btn.Location = new System.Drawing.Point(209, 182);
            this.triangleScaleUp_btn.Name = "triangleScaleUp_btn";
            this.triangleScaleUp_btn.Size = new System.Drawing.Size(161, 27);
            this.triangleScaleUp_btn.TabIndex = 11;
            this.triangleScaleUp_btn.Text = "Triangle Scale Up";
            this.triangleScaleUp_btn.UseVisualStyleBackColor = true;
            this.triangleScaleUp_btn.Click += new System.EventHandler(this.triangleScaleUp_btn_Click);
            // 
            // changeColorTriangle_btn
            // 
            this.changeColorTriangle_btn.AutoSize = true;
            this.changeColorTriangle_btn.Location = new System.Drawing.Point(209, 149);
            this.changeColorTriangle_btn.Name = "changeColorTriangle_btn";
            this.changeColorTriangle_btn.Size = new System.Drawing.Size(161, 27);
            this.changeColorTriangle_btn.TabIndex = 10;
            this.changeColorTriangle_btn.Text = "Change Color Triangle";
            this.changeColorTriangle_btn.UseVisualStyleBackColor = true;
            this.changeColorTriangle_btn.Click += new System.EventHandler(this.changeColorTriangle_btn_Click);
            // 
            // stopRotationTriangle_btn
            // 
            this.stopRotationTriangle_btn.AutoSize = true;
            this.stopRotationTriangle_btn.Location = new System.Drawing.Point(209, 116);
            this.stopRotationTriangle_btn.Name = "stopRotationTriangle_btn";
            this.stopRotationTriangle_btn.Size = new System.Drawing.Size(160, 27);
            this.stopRotationTriangle_btn.TabIndex = 9;
            this.stopRotationTriangle_btn.Text = "Stop Triangle Rotation";
            this.stopRotationTriangle_btn.UseVisualStyleBackColor = true;
            this.stopRotationTriangle_btn.Click += new System.EventHandler(this.stopRotationTriangle_btn_Click);
            // 
            // startTriangleRotation_btn
            // 
            this.startTriangleRotation_btn.AutoSize = true;
            this.startTriangleRotation_btn.Location = new System.Drawing.Point(208, 45);
            this.startTriangleRotation_btn.Name = "startTriangleRotation_btn";
            this.startTriangleRotation_btn.Size = new System.Drawing.Size(161, 27);
            this.startTriangleRotation_btn.TabIndex = 8;
            this.startTriangleRotation_btn.Text = "Rotate Triangle Left";
            this.startTriangleRotation_btn.UseVisualStyleBackColor = true;
            this.startTriangleRotation_btn.Click += new System.EventHandler(this.startTriangleRotation_btn_Click);
            // 
            // addTriangle_btn
            // 
            this.addTriangle_btn.AutoSize = true;
            this.addTriangle_btn.Location = new System.Drawing.Point(239, 10);
            this.addTriangle_btn.Name = "addTriangle_btn";
            this.addTriangle_btn.Size = new System.Drawing.Size(99, 27);
            this.addTriangle_btn.TabIndex = 7;
            this.addTriangle_btn.Text = "Add Triangle";
            this.addTriangle_btn.UseVisualStyleBackColor = true;
            this.addTriangle_btn.Click += new System.EventHandler(this.addTriangle_btn_Click);
            // 
            // scale_up_btn
            // 
            this.scale_up_btn.AutoSize = true;
            this.scale_up_btn.Location = new System.Drawing.Point(13, 182);
            this.scale_up_btn.Name = "scale_up_btn";
            this.scale_up_btn.Size = new System.Drawing.Size(159, 27);
            this.scale_up_btn.TabIndex = 6;
            this.scale_up_btn.Text = "Square Scale Up";
            this.scale_up_btn.UseVisualStyleBackColor = true;
            this.scale_up_btn.Click += new System.EventHandler(this.scale_up_btn_Click);
            // 
            // help_btn
            // 
            this.help_btn.AutoSize = true;
            this.help_btn.Location = new System.Drawing.Point(740, 433);
            this.help_btn.Name = "help_btn";
            this.help_btn.Size = new System.Drawing.Size(47, 28);
            this.help_btn.TabIndex = 7;
            this.help_btn.Text = "Help";
            this.help_btn.UseVisualStyleBackColor = true;
            this.help_btn.Click += new System.EventHandler(this.help_btn_Click);
            // 
            // duplicateSquare_btn
            // 
            this.duplicateSquare_btn.AutoSize = true;
            this.duplicateSquare_btn.Location = new System.Drawing.Point(623, 11);
            this.duplicateSquare_btn.Name = "duplicateSquare_btn";
            this.duplicateSquare_btn.Size = new System.Drawing.Size(127, 27);
            this.duplicateSquare_btn.TabIndex = 23;
            this.duplicateSquare_btn.Text = "Duplicate Square";
            this.duplicateSquare_btn.UseVisualStyleBackColor = true;
            this.duplicateSquare_btn.Click += new System.EventHandler(this.duplicateSquare_btn_Click);
            // 
            // duplicateTriangle_btn
            // 
            this.duplicateTriangle_btn.AutoSize = true;
            this.duplicateTriangle_btn.Location = new System.Drawing.Point(620, 45);
            this.duplicateTriangle_btn.Name = "duplicateTriangle_btn";
            this.duplicateTriangle_btn.Size = new System.Drawing.Size(133, 27);
            this.duplicateTriangle_btn.TabIndex = 24;
            this.duplicateTriangle_btn.Text = "Duplicate Triangle";
            this.duplicateTriangle_btn.UseVisualStyleBackColor = true;
            this.duplicateTriangle_btn.Click += new System.EventHandler(this.duplicateTriangle_btn_Click);
            // 
            // duplicateCircle_btn
            // 
            this.duplicateCircle_btn.AutoSize = true;
            this.duplicateCircle_btn.Location = new System.Drawing.Point(628, 78);
            this.duplicateCircle_btn.Name = "duplicateCircle_btn";
            this.duplicateCircle_btn.Size = new System.Drawing.Size(116, 27);
            this.duplicateCircle_btn.TabIndex = 25;
            this.duplicateCircle_btn.Text = "Duplicate Circle";
            this.duplicateCircle_btn.UseVisualStyleBackColor = true;
            this.duplicateCircle_btn.Click += new System.EventHandler(this.duplicateCircle_btn_Click);
            // 
            // Application
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 726);
            this.Controls.Add(this.help_btn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Application";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Application";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button addSquare_btn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button stop_square_rotation;
        private System.Windows.Forms.Button changeColor_txt;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button scale_up_btn;
        private System.Windows.Forms.Button help_btn;
        private System.Windows.Forms.Button triangleScaleUp_btn;
        private System.Windows.Forms.Button changeColorTriangle_btn;
        private System.Windows.Forms.Button stopRotationTriangle_btn;
        private System.Windows.Forms.Button startTriangleRotation_btn;
        private System.Windows.Forms.Button addTriangle_btn;
        private System.Windows.Forms.Button circleScaleUp_btn;
        private System.Windows.Forms.Button changeColorCircle_btn;
        private System.Windows.Forms.Button stopCircleRotation_btn;
        private System.Windows.Forms.Button startCircleRotation_btn;
        private System.Windows.Forms.Button addCircle_btn;
        private System.Windows.Forms.Button rotateCircleRight;
        private System.Windows.Forms.Button rotateTriangleRight_btn;
        private System.Windows.Forms.Button rotateSquareRight_btn;
        private System.Windows.Forms.Button squareScaleDown_btn;
        private System.Windows.Forms.Button triangleScaleDown_btn;
        private System.Windows.Forms.Button circleScaleDown_btn;
        private System.Windows.Forms.Button duplicateCircle_btn;
        private System.Windows.Forms.Button duplicateTriangle_btn;
        private System.Windows.Forms.Button duplicateSquare_btn;
    }
}

