﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255_1150825
{
    public partial class Application : Form
    {
        public int indexX = 0;
        public int indexY = 0;
        public int dividerX = 0;
        public int dividerY = 0;

        public int actual_index = -1;

        public List<Shape> history = new List<Shape>();

        public Timer rotationTimerSquareLeft;
        public Timer rotationTimerTriangleLeft;
        public Timer rotationTimerCircleLeft;

        public Timer rotationTimerSquareRight;
        public Timer rotationTimerTriangleRight;
        public Timer rotationTimerCircleRight;

        string background = "../../background.JPG";
        Bitmap margin = null;

        public Application()
        {
            InitializeComponent();

            margin = new Bitmap(Image.FromFile(background), 55, 55);

            this.Size = new Size(615, 420);

            this.panel1.Visible = false;

            dividerX = pictureBox1.Width / 8;
            dividerY = pictureBox1.Height / 4;

            setBackground();

            rotationTimerSquareLeft = new Timer();
            rotationTimerSquareLeft.Interval = 700;    //you can change it to handle smoothness
            rotationTimerSquareLeft.Tick += rotationTimerSquareLeft_Tick;

            rotationTimerTriangleLeft = new Timer();
            rotationTimerTriangleLeft.Interval = 700;    //you can change it to handle smoothness
            rotationTimerTriangleLeft.Tick += rotationTimerTriangleLeft_Tick;

            rotationTimerCircleLeft = new Timer();
            rotationTimerCircleLeft.Interval = 700;    //you can change it to handle smoothness
            rotationTimerCircleLeft.Tick += rotationTimerCircleLeft_Tick;

            rotationTimerSquareRight = new Timer();
            rotationTimerSquareRight.Interval = 700;    //you can change it to handle smoothness
            rotationTimerSquareRight.Tick += rotationTimerSquareRight_Tick;

            rotationTimerTriangleRight = new Timer();
            rotationTimerTriangleRight.Interval = 700;    //you can change it to handle smoothness
            rotationTimerTriangleRight.Tick += rotationTimerTriangleRight_Tick;

            rotationTimerCircleRight = new Timer();
            rotationTimerCircleRight.Interval = 700;    //you can change it to handle smoothness
            rotationTimerCircleRight.Tick += rotationTimerCircleRight_Tick;

        }

        public void setBackground()
        {
            string background = "../../background.JPG";
            Bitmap bitmap = new Bitmap(Image.FromFile(background), pictureBox1.Width, pictureBox1.Height);
            pictureBox1.Image = bitmap;
        }

        private void addSquare_btn_Click(object sender, EventArgs e)
        {
            addSquare(Color.Black);
        }

        int point_auxX = 0;
        int point_auxY = 0;

        public void addSquare(Color color)
        {
            string square_path = "../../shapes/square.JPG";
            Bitmap img = new Bitmap(Image.FromFile(square_path), 35, 35);

            point_auxX = indexX + (dividerX / 4);
            point_auxY = indexY + (dividerY / 4);

            if (checkAddShape(point_auxX, point_auxY) == true)
            {
                actual_index++;
                history.Add(new Shape(addMargin(margin, img), "square", point_auxX, point_auxY, color));
                pictureBox1.Image = refreshPictureBox();
            }
            addShape();
        }

        public Bitmap addMargin(Bitmap margin, Bitmap shape)
        {
            Bitmap rotated = new Bitmap(margin.Width, margin.Height);

            using (Graphics g = Graphics.FromImage(rotated))
            {
                Point center = new Point((margin.Width - shape.Width) / 2, (margin.Height - shape.Height) / 2);
                center.X = center.X < 0 ? 0 : center.X;
                center.Y = center.Y < 0 ? 0 : center.Y;
                g.DrawImage(shape, center);
            }
            return rotated;
        }

        private Bitmap refreshPictureBox()
        {
            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    Shape shape = history[actual_index];
                    Bitmap img = (Bitmap)shape.image;

                    g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                        ? img.Size.Width : pictureBox1.Image.Size.Width,
                        (img.Size.Height < pictureBox1.Image.Size.Height)
                        ? img.Size.Height : pictureBox1.Image.Size.Height));
                }
            }
            return layeredImg;
        }

        public Boolean checkAddShape(int auxX, int auxY)
        {
            if (auxX < pictureBox1.Width && auxY < pictureBox1.Height)
            {
                return true;
            }
            return false;
        }

        private void addShape()
        {
            int auxX = indexX + dividerX;
            int auxY = indexY + dividerY;

            if (auxX + dividerX < pictureBox1.Width)
            {
                indexX += dividerX;
            }
            else
            {
                if (auxX + dividerX > pictureBox1.Width)
                {
                    indexX = 0;
                    indexY += dividerY;
                }
                else
                {
                    if (auxY + dividerY > pictureBox1.Height)
                    {
                        indexY = 0;
                        indexX += dividerX;
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (rotationTimerSquareRight.Enabled)
            {
                rotationTimerSquareRight.Stop();
            }
            rotationTimerSquareLeft.Start();
        }

        void rotationTimerSquareLeft_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeLeft();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerSquareRight_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeRight();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);

                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerTriangleLeft_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeLeft();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerTriangleRight_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeRight();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerCircleLeft_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeLeft();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerCircleRight_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeRight();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void stop_square_rotation_Click(object sender, EventArgs e)
        {
            if (rotationTimerSquareLeft.Enabled)
            {
                rotationTimerSquareLeft.Stop();
            }
            else
            {
                if (rotationTimerSquareRight.Enabled)
                {
                    rotationTimerSquareRight.Stop();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SpeechToText form = new SpeechToText(this);
            form.ShowDialog();
        }

        private void changeColor_txt_Click(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            Bitmap img = (Bitmap)shape.changeColor(Color.Red);

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                   (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void help_btn_Click(object sender, EventArgs e)
        {
            this.Size = new Size(615, 630);
            this.panel1.Visible = true;
        }

        private void scale_up_btn_Click(object sender, EventArgs e)
        {
            this.scaleUpSquare();
        }

        private void addTriangle_btn_Click(object sender, EventArgs e)
        {
            addTriangle(Color.Black);
        }

        public void addTriangle(Color color)
        {
            string triangle_path = "../../shapes/triangle.JPG";
            Bitmap img = new Bitmap(Image.FromFile(triangle_path), 35, 35);

            point_auxX = indexX + (dividerX / 4);
            point_auxY = indexY + (dividerY / 4);

            if (checkAddShape(point_auxX, point_auxY) == true)
            {
                actual_index++;
                history.Add(new Shape(addMargin(margin, img), "triangle", point_auxX, point_auxY, color));
                pictureBox1.Image = refreshPictureBox();
            }
            addShape();
        }

        private void startTriangleRotation_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerTriangleRight.Enabled)
            {
                rotationTimerTriangleRight.Stop();
            }
            rotationTimerTriangleLeft.Start();
        }

        private void stopRotationTriangle_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerTriangleLeft.Enabled)
            {
                rotationTimerTriangleLeft.Stop();
            }
            else
            {
                if (rotationTimerTriangleRight.Enabled)
                {
                    rotationTimerTriangleRight.Stop();
                }
            }
        }

        private void changeColorTriangle_btn_Click(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            Bitmap img = (Bitmap)shape.changeColor(Color.Green);

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            g.DrawImage(shape.image, shape.x, shape.y, shape.image.Width, shape.image.Height);
                        }
                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void addCircle_btn_Click(object sender, EventArgs e)
        {
            addCircle(Color.Black);
        }

        public void addCircle(Color color)
        {
            string triangle_path = "../../shapes/circle.JPG";
            Bitmap img = new Bitmap(Image.FromFile(triangle_path), 35, 35);

            point_auxX = indexX + (dividerX / 4);
            point_auxY = indexY + (dividerY / 4);

            if (checkAddShape(point_auxX, point_auxY) == true)
            {
                actual_index++;
                history.Add(new Shape(addMargin(margin, img), "circle", point_auxX, point_auxY, color));
                pictureBox1.Image = refreshPictureBox();
            }
            addShape();
        }

        private void startCircleRotation_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerCircleRight.Enabled)
            {
                rotationTimerCircleRight.Stop();
            }
            rotationTimerCircleLeft.Start();
        }

        private void stopCircleRotation_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerCircleLeft.Enabled)
            {
                rotationTimerCircleLeft.Stop();
            }
            else
            {
                if (rotationTimerCircleRight.Enabled)
                {
                    rotationTimerCircleRight.Stop();
                }
            }
        }

        private void changeColorCircle_btn_Click(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            Bitmap img = (Bitmap)shape.changeColor(Color.Green);

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            g.DrawImage(shape.image, shape.x, shape.y, shape.image.Width, shape.image.Height);
                        }
                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void changeColorSquare(Color color)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            if (shape.color == color)
                            {
                                MessageBox.Show("Color already selected");
                            }
                            else
                            {
                                Bitmap img = (Bitmap)shape.changeColor(color);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                   (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void changeColorTriangle(Color color)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            if (shape.color == color)
                            {
                                MessageBox.Show("Color already selected");
                            }
                            else
                            {
                                Bitmap img = (Bitmap)shape.changeColor(color);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                   (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void changeColorCircle(Color color)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            if (shape.color == color)
                            {
                                MessageBox.Show("Color already selected");
                            }
                            else
                            {
                                Bitmap img = (Bitmap)shape.changeColor(color);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                   (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void rotateSquareRight_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerSquareLeft.Enabled)
            {
                rotationTimerSquareLeft.Stop();
            }
            rotationTimerSquareRight.Start();
        }

        private void rotateTriangleRight_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerTriangleLeft.Enabled)
            {
                rotationTimerTriangleLeft.Stop();
            }
            rotationTimerTriangleRight.Start();
        }

        private void rotateCircleRight_Click(object sender, EventArgs e)
        {
            if (rotationTimerCircleLeft.Enabled)
            {
                rotationTimerCircleLeft.Stop();
            }
            rotationTimerCircleRight.Start();
        }

        private void triangleScaleUp_btn_Click(object sender, EventArgs e)
        {
            this.scaleUpTriangle();
        }

        private void circleScaleUp_btn_Click(object sender, EventArgs e)
        {
            this.scaleUpCircle();
        }

        private void squareScaleDown_btn_Click(object sender, EventArgs e)
        {
            this.scaleDownSquare();
        }

        private void triangleScaleDown_btn_Click(object sender, EventArgs e)
        {
            this.scaleDownTriangle();
        }

        private void circleScaleDown_btn_Click(object sender, EventArgs e)
        {
            this.scaleDownCircle();
        }

        public void scaleUpSquare()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            if (shape.margin >= 60)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleUp();

                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleUpTriangle()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            if (shape.margin >= 60)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleUp();
                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleUpCircle()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            if (shape.margin >= 60)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleUp();

                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);

                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleDownSquare()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            if (shape.margin <= 50)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleDown();

                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleDownTriangle()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            if (shape.margin <= 50)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleDown();
                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleDownCircle()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            if (shape.margin <= 50)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleDown();

                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);

                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void duplicateSquare_btn_Click(object sender, EventArgs e)
        {
            duplicateSquare();
        }

        public void duplicateSquare()
        {
            int count = 0;
            foreach (Shape shape in history)
            {
                if (shape.shape == "square")
                {
                    count++;
                }
            }

            for(int i=0; i<count; i++)
            {
                addSquare(Color.Black);
            }

            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        g.DrawImage((Bitmap)shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                             ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                             (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                             ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void duplicateTriangle()
        {
            int count = 0;
            foreach (Shape shape in history)
            {
                if (shape.shape == "triangle")
                {
                    count++;
                }
            }

            for (int i = 0; i < count; i++)
            {
                addTriangle(Color.Black);
            }

            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        g.DrawImage((Bitmap)shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                             ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                             (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                             ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void duplicateCircle()
        {
            int count = 0;
            foreach (Shape shape in history)
            {
                if (shape.shape == "circle")
                {
                    count++;
                }
            }

            for (int i = 0; i < count; i++)
            {
                addCircle(Color.Black);
            }

            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        g.DrawImage((Bitmap)shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                             ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                             (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                             ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void duplicateTriangle_btn_Click(object sender, EventArgs e)
        {
            duplicateTriangle();
        }

        private void duplicateCircle_btn_Click(object sender, EventArgs e)
        {
            duplicateCircle();
        }

    }
}
