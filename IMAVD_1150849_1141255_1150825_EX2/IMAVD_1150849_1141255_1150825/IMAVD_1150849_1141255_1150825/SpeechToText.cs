﻿using Google.Cloud.Speech.V1;
using NAudio.Wave;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255_1150825
{
    public partial class SpeechToText : Form
    {

        private BufferedWaveProvider bwp;

        WaveIn waveIn;
        WaveOut waveOut;
        WaveFileWriter writer;
        WaveFileReader reader;
        string output = "audio.raw";

        Application form1;

        public SpeechToText(Application form1)
        {
            InitializeComponent();

            this.form1 = form1;

            waveOut = new WaveOut();
            waveIn = new WaveIn();

            waveIn.DataAvailable += new EventHandler<WaveInEventArgs>(waveIn_DataAvailable);
            waveIn.WaveFormat = new NAudio.Wave.WaveFormat(16000, 1);
            bwp = new BufferedWaveProvider(waveIn.WaveFormat);
            bwp.DiscardOnBufferOverflow = true;

            record_btn.Enabled = true;
            save_btn.Enabled = false;
            convert_btn.Enabled = false;
            apply_btn.Enabled = false;
        }



        private void btn_record_Click(object sender, EventArgs e)
        {
            if (NAudio.Wave.WaveIn.DeviceCount < 1)
            {
                Console.WriteLine("No microphone!");
                return;
            }


            waveOut = new WaveOut();
            waveIn = new WaveIn();

            waveIn.DataAvailable += new EventHandler<WaveInEventArgs>(waveIn_DataAvailable);
            waveIn.WaveFormat = new NAudio.Wave.WaveFormat(16000, 1);
            bwp = new BufferedWaveProvider(waveIn.WaveFormat);
            bwp.DiscardOnBufferOverflow = true;


            waveIn.StartRecording();

            record_btn.Enabled = false;
            save_btn.Enabled = true;
            convert_btn.Enabled = false;
            apply_btn.Enabled = false;
        }

        private void playAudio_btn_Click(object sender, EventArgs e)
        {
            if (File.Exists("audio.raw"))
            {
                reader = new WaveFileReader("audio.raw");
                waveOut.Init(reader);
                waveOut.Play();
            }
            else
            {
                MessageBox.Show("No Audio File Found");
            }
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            waveIn.StopRecording();

            if (File.Exists("audio.raw"))
            {
                //File.Delete("audio.raw");
            }

            writer = new WaveFileWriter(output, waveIn.WaveFormat);

            record_btn.Enabled = true;
            save_btn.Enabled = false;
            convert_btn.Enabled = true;

            byte[] buffer = new byte[bwp.BufferLength];
            int offset = 0;
            int count = bwp.BufferLength;

            var read = bwp.Read(buffer, offset, count);
            if (count > 0)
            {
                writer.Write(buffer, offset, read);
            }

            waveIn.Dispose();
            waveIn = null;
            writer.Close();
            writer = null;

        }

        private void convert_btn_Click(object sender, EventArgs e)
        {
            record_btn.Enabled = false;
            save_btn.Enabled = false;
            convert_btn.Enabled = false;

            if (File.Exists("audio.raw"))
            {

                var speech = SpeechClient.Create();
                var response = speech.Recognize(new RecognitionConfig()
                {
                    Encoding = RecognitionConfig.Types.AudioEncoding.Linear16,
                    SampleRateHertz = 16000,
                    LanguageCode = "en",
                }, RecognitionAudio.FromFile("audio.raw"));


                textBox1.Text = "";

                foreach (var result in response.Results)
                {
                    foreach (var alternative in result.Alternatives)
                    {
                        textBox1.Text = textBox1.Text + " " + alternative.Transcript;
                    }
                }

                if (textBox1.Text.Length == 0)
                    textBox1.Text = "No Data ";

            }
            else
            {
                textBox1.Text = "Audio File Missing ";
            }

            record_btn.Enabled = true;
            save_btn.Enabled = false;
            convert_btn.Enabled = false;
            apply_btn.Enabled = true;
        }

        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            bwp.AddSamples(e.Buffer, 0, e.BytesRecorded);

        }

        private void waveOut_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            waveOut.Stop();
            reader.Close();
            reader = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string textFromTextBox = textBox1.Text.ToLower().Trim();

            switch (textFromTextBox)
            {
                case "add square":
                    form1.addSquare(Color.Black);
                    this.Close();
                    break;
                case "rotate square left":
                    if (form1.rotationTimerSquareRight.Enabled)
                    {
                        form1.rotationTimerSquareRight.Stop();
                    }
                    form1.rotationTimerSquareLeft.Start();
                    this.Close();
                    break;
                case "rotate square right":
                    if (form1.rotationTimerSquareLeft.Enabled)
                    {
                        form1.rotationTimerSquareLeft.Stop();
                    }
                    form1.rotationTimerSquareRight.Start();
                    this.Close();
                    break;
                case "stop square rotation":
                    if (form1.rotationTimerSquareRight.Enabled)
                    {
                        form1.rotationTimerSquareRight.Stop();
                    }
                    else
                    {
                        if (form1.rotationTimerSquareLeft.Enabled)
                        {
                            form1.rotationTimerSquareLeft.Stop();
                        }
                    }
                    this.Close();
                    break;
                case "change square color green":
                    form1.changeColorSquare(Color.Green);
                    this.Close();
                    break;
                case "change square color blue":
                    form1.changeColorSquare(Color.Blue);
                    this.Close();
                    break;
                case "change square color red":
                    form1.changeColorSquare(Color.Red);
                    this.Close();
                    break;
                case "change square color yellow":
                    form1.changeColorSquare(Color.Yellow);
                    this.Close();
                    break;
                case "change square color black":
                    form1.changeColorSquare(Color.Black);
                    this.Close();
                    break;
                case "scale up square":
                    form1.scaleUpSquare();
                    this.Close();
                    break;
                case "scale down square":
                    form1.scaleDownSquare();
                    this.Close();
                    break;
                case "duplicate square":
                    form1.duplicateSquare();
                    this.Close();
                    break;

                case "add triangle":
                    form1.addTriangle(Color.Black);
                    this.Close();
                    break;
                case "rotate triangle left":
                    if (form1.rotationTimerTriangleRight.Enabled)
                    {
                        form1.rotationTimerTriangleRight.Stop();
                    }
                    form1.rotationTimerTriangleLeft.Start();
                    this.Close();
                    break;
                case "rotate triangle right":
                    if (form1.rotationTimerTriangleLeft.Enabled)
                    {
                        form1.rotationTimerTriangleLeft.Stop();
                    }
                    form1.rotationTimerTriangleRight.Start();
                    this.Close();
                    break;
                case "stop triangle rotation":
                    if (form1.rotationTimerTriangleLeft.Enabled)
                    {
                        form1.rotationTimerTriangleLeft.Stop();
                    }
                    else
                    {
                        if (form1.rotationTimerTriangleRight.Enabled)
                        {
                            form1.rotationTimerTriangleRight.Stop();
                        }
                    }
                    this.Close();
                    break;
                case "change triangle color green":
                    form1.changeColorTriangle(Color.Green);
                    this.Close();
                    break;
                case "change triangle color blue":
                    form1.changeColorTriangle(Color.Blue);
                    this.Close();
                    break;
                case "change triangle color red":
                    form1.changeColorTriangle(Color.Red);
                    this.Close();
                    break;
                case "change triangle color yellow":
                    form1.changeColorTriangle(Color.Yellow);
                    this.Close();
                    break;
                case "change triangle color black":
                    form1.changeColorTriangle(Color.Black);
                    this.Close();
                    break;
                case "scale up triangle":
                    form1.scaleUpTriangle();
                    this.Close();
                    break;
                case "scale down triangle":
                    form1.scaleDownTriangle();
                    this.Close();
                    break;
                case "duplicate triangle":
                    form1.duplicateTriangle();
                    this.Close();
                    break;

                case "add circle":
                    form1.addCircle(Color.Black);
                    this.Close();
                    break;
                case "rotate circle left":
                    if (form1.rotationTimerCircleRight.Enabled)
                    {
                        form1.rotationTimerTriangleRight.Stop();
                    }
                    form1.rotationTimerTriangleLeft.Start();
                    this.Close();
                    break;
                case "rotate circle right":
                    if (form1.rotationTimerCircleLeft.Enabled)
                    {
                        form1.rotationTimerTriangleLeft.Stop();
                    }
                    form1.rotationTimerTriangleRight.Start();
                    this.Close();
                    break;
                case "stop circle rotation":
                    if (form1.rotationTimerTriangleRight.Enabled)
                    {
                        form1.rotationTimerTriangleRight.Stop();
                    }
                    else
                    {
                        if (form1.rotationTimerTriangleLeft.Enabled)
                        {
                            form1.rotationTimerCircleLeft.Stop();
                        }
                    }
                    this.Close();
                    break;
                case "change circle color green":
                    form1.changeColorCircle(Color.Green);
                    this.Close();
                    break;
                case "change circle color blue":
                    form1.changeColorCircle(Color.Blue);
                    this.Close();
                    break;
                case "change circle color red":
                    form1.changeColorCircle(Color.Red);
                    this.Close();
                    break;
                case "change circle color yellow":
                    form1.changeColorCircle(Color.Yellow);
                    this.Close();
                    break;
                case "change circle color black":
                    form1.changeColorCircle(Color.Black);
                    this.Close();
                    break;
                case "scale up circle":
                    form1.scaleUpCircle();
                    this.Close();
                    break;
                case "scale down circle":
                    form1.scaleDownCircle();
                    this.Close();
                    break;
                case "duplicate circle":
                    form1.duplicateCircle();
                    this.Close();
                    break;

                case "add two blue squares and rotate left":
                    form1.addSquare(Color.Blue);
                    form1.addSquare(Color.Blue);
                    if (form1.rotationTimerSquareRight.Enabled)
                    {
                        form1.rotationTimerSquareRight.Stop();
                    }
                    form1.rotationTimerSquareLeft.Start();
                    this.Close();
                    break;

                default:
                    break;
            }
        }

    }
}
