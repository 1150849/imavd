﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace IMAVD_1150849_1141255_1150825
{
    public partial class Application : Form
    {
        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        public int indexX = 0;
        public int indexY = 0;
        public int dividerX = 0;
        public int dividerY = 0;

        public int actual_index = -1;

        public List<Shape> history = new List<Shape>();

        public Timer rotationTimerSquareLeft;
        public Timer rotationTimerTriangleLeft;
        public Timer rotationTimerCircleLeft;

        public Timer rotationTimerSquareRight;
        public Timer rotationTimerTriangleRight;
        public Timer rotationTimerCircleRight;

        string background = "../../background.JPG";
        Bitmap margin = null;

        private VideoCapture capture = null;

        private Boolean captureFaceProgress;
        private Boolean captureShapeProgress;

        CascadeClassifier detectFace = new CascadeClassifier("../../lib/haarcascade_frontalface_default.xml");

        public Timer timer1;
        public Timer timer2;

        public String path = "def";

        private StopSignDetector stopSignDetector;

        Mat imag = CvInvoke.Imread(@"../../images/stop-sign.jpg");

        public Application()
        {
            InitializeComponent();

            LoadWindow();

            margin = new Bitmap(Image.FromFile(background), 55, 55);

            this.Size = new Size(950, 690);

            dividerX = pictureBox1.Width / 8;
            dividerY = pictureBox1.Height / 4;

            setBackground();

            rotationTimerSquareLeft = new Timer();
            rotationTimerSquareLeft.Interval = 700;    //you can change it to handle smoothness
            rotationTimerSquareLeft.Tick += rotationTimerSquareLeft_Tick;

            rotationTimerTriangleLeft = new Timer();
            rotationTimerTriangleLeft.Interval = 700;    //you can change it to handle smoothness
            rotationTimerTriangleLeft.Tick += rotationTimerTriangleLeft_Tick;

            rotationTimerCircleLeft = new Timer();
            rotationTimerCircleLeft.Interval = 700;    //you can change it to handle smoothness
            rotationTimerCircleLeft.Tick += rotationTimerCircleLeft_Tick;

            rotationTimerSquareRight = new Timer();
            rotationTimerSquareRight.Interval = 700;    //you can change it to handle smoothness
            rotationTimerSquareRight.Tick += rotationTimerSquareRight_Tick;

            rotationTimerTriangleRight = new Timer();
            rotationTimerTriangleRight.Interval = 700;    //you can change it to handle smoothness
            rotationTimerTriangleRight.Tick += rotationTimerTriangleRight_Tick;

            rotationTimerCircleRight = new Timer();
            rotationTimerCircleRight.Interval = 700;    //you can change it to handle smoothness
            rotationTimerCircleRight.Tick += rotationTimerCircleRight_Tick;
        }

        public void setBackground()
        {
            string background = "../../background.JPG";
            Bitmap bitmap = new Bitmap(Image.FromFile(background), pictureBox1.Width, pictureBox1.Height);
            pictureBox1.Image = bitmap;
        }

        private void addSquare_btn_Click(object sender, EventArgs e)
        {
            addSquare(Color.Black);
        }

        int point_auxX = 0;
        int point_auxY = 0;

        public void addSquare(Color color)
        {
            string square_path = "../../shapes/square.JPG";
            Bitmap img = new Bitmap(Image.FromFile(square_path), 35, 35);

            point_auxX = indexX + (dividerX / 4);
            point_auxY = indexY + (dividerY / 4);

            if (checkAddShape(point_auxX, point_auxY) == true)
            {
                actual_index++;
                history.Add(new Shape(addMargin(margin, img), "square", point_auxX, point_auxY, color));
                pictureBox1.Image = refreshPictureBox();
            }
            addShape();
        }

        public Bitmap addMargin(Bitmap margin, Bitmap shape)
        {
            Bitmap rotated = new Bitmap(margin.Width, margin.Height);

            using (Graphics g = Graphics.FromImage(rotated))
            {
                Point center = new Point((margin.Width - shape.Width) / 2, (margin.Height - shape.Height) / 2);
                center.X = center.X < 0 ? 0 : center.X;
                center.Y = center.Y < 0 ? 0 : center.Y;
                g.DrawImage(shape, center);
            }
            return rotated;
        }

        private Bitmap refreshPictureBox()
        {
            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    Shape shape = history[actual_index];
                    Bitmap img = (Bitmap)shape.image;

                    g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                        ? img.Size.Width : pictureBox1.Image.Size.Width,
                        (img.Size.Height < pictureBox1.Image.Size.Height)
                        ? img.Size.Height : pictureBox1.Image.Size.Height));
                }
            }
            return layeredImg;
        }

        public Boolean checkAddShape(int auxX, int auxY)
        {
            if (auxX < pictureBox1.Width && auxY < pictureBox1.Height)
            {
                return true;
            }
            return false;
        }

        private void addShape()
        {
            int auxX = indexX + dividerX;
            int auxY = indexY + dividerY;

            if (auxX + dividerX < pictureBox1.Width)
            {
                indexX += dividerX;
            }
            else
            {
                if (auxX + dividerX > pictureBox1.Width)
                {
                    indexX = 0;
                    indexY += dividerY;
                }
                else
                {
                    if (auxY + dividerY > pictureBox1.Height)
                    {
                        indexY = 0;
                        indexX += dividerX;
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (rotationTimerSquareRight.Enabled)
            {
                rotationTimerSquareRight.Stop();
            }
            rotationTimerSquareLeft.Start();
        }

        void rotationTimerSquareLeft_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeLeft();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerSquareRight_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeRight();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);

                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerTriangleLeft_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeLeft();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerTriangleRight_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeRight();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerCircleLeft_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeLeft();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        void rotationTimerCircleRight_Tick(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            Bitmap img = (Bitmap)shape.rotateShapeRight();

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            Bitmap img = (Bitmap)shape.atualRotation();

                            g.DrawImage(img, new Rectangle(shape.x, shape.y, (img.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? img.Size.Width : pictureBox1.Image.Size.Width,
                                                   (img.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? img.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void stop_square_rotation_Click(object sender, EventArgs e)
        {
            if (rotationTimerSquareLeft.Enabled)
            {
                rotationTimerSquareLeft.Stop();
            }
            else
            {
                if (rotationTimerSquareRight.Enabled)
                {
                    rotationTimerSquareRight.Stop();
                }
            }
        }

        private void changeColor_txt_Click(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            Bitmap img = (Bitmap)shape.changeColor(Color.Red);

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                   (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void scale_up_btn_Click(object sender, EventArgs e)
        {
            this.scaleUpSquare();
        }

        private void addTriangle_btn_Click(object sender, EventArgs e)
        {
            addTriangle(Color.Black);
        }

        public void addTriangle(Color color)
        {
            string triangle_path = "../../shapes/triangle.JPG";
            Bitmap img = new Bitmap(Image.FromFile(triangle_path), 35, 35);

            point_auxX = indexX + (dividerX / 4);
            point_auxY = indexY + (dividerY / 4);

            if (checkAddShape(point_auxX, point_auxY) == true)
            {
                actual_index++;
                history.Add(new Shape(addMargin(margin, img), "triangle", point_auxX, point_auxY, color));
                pictureBox1.Image = refreshPictureBox();
            }
            addShape();
        }

        private void startTriangleRotation_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerTriangleRight.Enabled)
            {
                rotationTimerTriangleRight.Stop();
            }
            rotationTimerTriangleLeft.Start();
        }

        private void stopRotationTriangle_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerTriangleLeft.Enabled)
            {
                rotationTimerTriangleLeft.Stop();
            }
            else
            {
                if (rotationTimerTriangleRight.Enabled)
                {
                    rotationTimerTriangleRight.Stop();
                }
            }
        }

        private void changeColorTriangle_btn_Click(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            Bitmap img = (Bitmap)shape.changeColor(Color.Green);

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            g.DrawImage(shape.image, shape.x, shape.y, shape.image.Width, shape.image.Height);
                        }
                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void addCircle_btn_Click(object sender, EventArgs e)
        {
            addCircle(Color.Black);
        }

        public void addCircle(Color color)
        {
            string triangle_path = "../../shapes/circle.JPG";
            Bitmap img = new Bitmap(Image.FromFile(triangle_path), 35, 35);

            point_auxX = indexX + (dividerX / 4);
            point_auxY = indexY + (dividerY / 4);

            if (checkAddShape(point_auxX, point_auxY) == true)
            {
                actual_index++;
                history.Add(new Shape(addMargin(margin, img), "circle", point_auxX, point_auxY, color));
                pictureBox1.Image = refreshPictureBox();
            }
            addShape();
        }

        private void startCircleRotation_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerCircleRight.Enabled)
            {
                rotationTimerCircleRight.Stop();
            }
            rotationTimerCircleLeft.Start();
        }

        private void stopCircleRotation_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerCircleLeft.Enabled)
            {
                rotationTimerCircleLeft.Stop();
            }
            else
            {
                if (rotationTimerCircleRight.Enabled)
                {
                    rotationTimerCircleRight.Stop();
                }
            }
        }

        private void changeColorCircle_btn_Click(object sender, EventArgs e)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            Bitmap img = (Bitmap)shape.changeColor(Color.Green);

                            g.InterpolationMode = InterpolationMode.NearestNeighbor;
                            g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                        }
                        else
                        {
                            g.DrawImage(shape.image, shape.x, shape.y, shape.image.Width, shape.image.Height);
                        }
                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void changeColorSquare(Color color)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            if (shape.color == color)
                            {
                                MessageBox.Show("Color already selected");
                            }
                            else
                            {
                                Bitmap img = (Bitmap)shape.changeColor(color);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                   (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void changeColorTriangle(Color color)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            if (shape.color == color)
                            {
                                MessageBox.Show("Color already selected");
                            }
                            else
                            {
                                Bitmap img = (Bitmap)shape.changeColor(color);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                   (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void changeColorCircle(Color color)
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            if (shape.color == color)
                            {
                                MessageBox.Show("Color already selected");
                            }
                            else
                            {
                                Bitmap img = (Bitmap)shape.changeColor(color);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(img, shape.x, shape.y, img.Width, img.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                                   ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                                   (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                                   ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        private void rotateSquareRight_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerSquareLeft.Enabled)
            {
                rotationTimerSquareLeft.Stop();
            }
            rotationTimerSquareRight.Start();
        }

        private void rotateTriangleRight_btn_Click(object sender, EventArgs e)
        {
            if (rotationTimerTriangleLeft.Enabled)
            {
                rotationTimerTriangleLeft.Stop();
            }
            rotationTimerTriangleRight.Start();
        }

        private void rotateCircleRight_Click(object sender, EventArgs e)
        {
            if (rotationTimerCircleLeft.Enabled)
            {
                rotationTimerCircleLeft.Stop();
            }
            rotationTimerCircleRight.Start();
        }

        private void triangleScaleUp_btn_Click(object sender, EventArgs e)
        {
            this.scaleUpTriangle();
        }

        private void circleScaleUp_btn_Click(object sender, EventArgs e)
        {
            this.scaleUpCircle();
        }

        private void squareScaleDown_btn_Click(object sender, EventArgs e)
        {
            this.scaleDownSquare();
        }

        private void triangleScaleDown_btn_Click(object sender, EventArgs e)
        {
            this.scaleDownTriangle();
        }

        private void circleScaleDown_btn_Click(object sender, EventArgs e)
        {
            this.scaleDownCircle();
        }

        public void scaleUpSquare()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            if (shape.margin >= 60)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleUp();

                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleUpTriangle()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            if (shape.margin >= 60)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleUp();
                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleUpCircle()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            if (shape.margin >= 60)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleUp();

                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);

                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleDownSquare()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "square")
                        {
                            if (shape.margin <= 50)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleDown();

                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleDownTriangle()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "triangle")
                        {
                            if (shape.margin <= 50)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleDown();
                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);
                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }

        public void scaleDownCircle()
        {
            setBackground();

            Bitmap layeredImg = new Bitmap(pictureBox1.Image);

            if (history.Count > 0)
            {
                using (Graphics g = Graphics.FromImage(layeredImg))
                {
                    foreach (Shape shape in history)
                    {
                        if (shape.shape == "circle")
                        {
                            if (shape.margin <= 50)
                            {
                                MessageBox.Show("Scale limit reached!");
                            }
                            else
                            {
                                Bitmap m = (Bitmap)shape.scaleDown();

                                shape.image = m;

                                //Bitmap m = addMargin(margin, img);

                                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                                g.DrawImage(m, shape.x, shape.y, m.Width, m.Height);

                            }
                        }
                        else
                        {
                            g.DrawImage(shape.image, new Rectangle(shape.x, shape.y, (shape.image.Size.Width < pictureBox1.Image.Size.Width)
                                        ? shape.image.Size.Width : pictureBox1.Image.Size.Width,
                                        (shape.image.Size.Height < pictureBox1.Image.Size.Height)
                                        ? shape.image.Size.Height : pictureBox1.Image.Size.Height));
                        }

                    }
                }
            }
            this.pictureBox1.Image = layeredImg;
        }



        private void LoadWindow()
        {
            try
            {
                capture = new VideoCapture();
                capture.Stop();
                captureFaceProgress = true;
                captureShapeProgress = true;

                if (captureFaceProgress == false)
                {
                    faceRecording_btn.Text = "Start Recording";
                }
                else
                {
                    faceRecording_btn.Text = "Stop Recording";
                }

                if (captureFaceProgress == false)
                {
                    faceRecording_btn.Text = "Start Recording";
                }
                else
                {
                    faceRecording_btn.Text = "Stop Recording";
                }
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show(ex.Message);
            }

            timer1 = new Timer();
            timer1.Interval = 10;    //you can change it to handle smoothness
            timer1.Tick += cameraFace_Tick;
            timer1.Start();

            timer2 = new Timer();
            timer2.Interval = 2000;    //you can change it to handle smoothness
            timer2.Tick += cameraShape_Tick;
            timer2.Start();
        }

        void cameraFace_Tick(object sender, EventArgs e)
        {
            try
            {
                using (Image<Bgr, Byte> currentFrame = capture.QueryFrame().ToImage<Bgr, byte>())
                {
                    if (currentFrame == null)
                    {
                        throw new Exception("No current frame!");
                    }

                    if (currentFrame != null && captureFaceProgress)
                    {
                        detectFaceHaar(currentFrame);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void cameraShape_Tick(object sender, EventArgs e)
        {
            try
            {
                using (Image<Bgr, Byte> currentFrame = capture.QueryFrame().ToImage<Bgr, byte>())
                {
                    if (currentFrame == null)
                    {
                        throw new Exception("No current frame!");
                    }

                    if (currentFrame != null && captureShapeProgress)
                    {
                        detectShape(currentFrame);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public Shape returnShape()
        {
            for (int i = 0; i < history.Count; i++)
            {
                if (history[i].shape == "square")
                {
                    return history[i];
                }
            }
            return null;
        }

        private void detectFaceHaar(Image<Bgr, byte> currentFrame)
        {
            string path = Path.GetFullPath(@"../../lib/haarcascade_frontalface_default.xml");

            CascadeClassifier classifierFace = new CascadeClassifier(path);

            try
            {
                Image<Gray, Byte> grayFrame = currentFrame.Convert<Gray, byte>().Clone();

                Rectangle[] detectedFaces = classifierFace.DetectMultiScale(grayFrame, 1.1, 4);

                if (detectedFaces.Length > 1)
                {
                    if (color == Color.Red && stopSign == true)
                    {
                        stopSign = false;
                        color = Color.Green;
                        //changeColorSquare(Color.Green);
                    }
                }

                if (stopSign == false)
                {
                    if (detectedFaces.Length > 0)
                    {
                        if (rotationTimerSquareRight.Enabled)
                        {
                            rotationTimerSquareRight.Stop();
                        }
                        else
                        {
                            if (!rotationTimerSquareLeft.Enabled)
                            {
                                rotationTimerSquareLeft.Start();
                            }
                        }
                    }
                    else
                    {
                        if (rotationTimerSquareLeft.Enabled)
                        {
                            rotationTimerSquareLeft.Stop();
                        }
                    }
                }

                foreach (var face in detectedFaces)
                {
                    currentFrame.Draw(face, new Bgr(0, 0, 255), 2);
                    //currentFrame.Draw(face, new Bgr(0, double.MaxValue, 0), 3);
                    grayFrame.ROI = face;
                }

                //Bitmap b = new Bitmap(currentFrame.ToBitmap(), image1.Width, image1.Height);

                Bitmap original = new Bitmap(currentFrame.ToBitmap(), image1.Width, image1.Height);

                original.RotateFlip(RotateFlipType.RotateNoneFlipX);

                image1.Image = original;

                grayFrame.Dispose();

                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        Boolean stopSign = false;
        Color color = Color.Black;
        public void detectShape(Image<Bgr, byte> currentFrame)
        {
            Image<Bgr, Byte> img = null;
            Bitmap original = null;

            //Load the image from file and resize it for display
            if (path == "def")
            {
                img = currentFrame.Clone();
            }
            else
            {
                img = new Image<Bgr, byte>(path);
            }

            stopSignDetector = new StopSignDetector();

            if (stopSignDetector.testSign(img) == true)
            {
                stopSignDetector = new StopSignDetector(img);

                List<Mat> stopSignList = new List<Mat>();
                List<Rectangle> stopSignBoxList = new List<Rectangle>();

                Boolean ssD = stopSignDetector.DetectStopSign(imag, stopSignList, stopSignBoxList);

                if (ssD == true)
                {
                    if (rotationTimerSquareLeft.Enabled)
                    {
                        rotationTimerSquareLeft.Stop();
                    }

                    if (color != Color.Red && stopSign == false)
                    {
                        changeColorSquare(Color.Red);
                        color = Color.Red;
                        stopSign = true;
                    }
                    MessageBox.Show("STOP Detected. Square rotation on hold!");
                }
                else
                {
                    checkShapes(img, original);
                }
            }
            else
            {
                checkShapes(img, original);
            }

        }

        private void checkShapes(Image<Bgr, Byte> img, Bitmap original)
        {
            //Convert the image to grayscale and filter out the noise
            UMat uimage = new UMat();
            CvInvoke.CvtColor(img, uimage, ColorConversion.Bgr2Gray);

            //use image pyr to remove noise
            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(uimage, pyrDown);
            CvInvoke.PyrUp(pyrDown, uimage);

            #region circle detection

            double cannyThreshold = 180.0;
            double circleAccumulatorThreshold = 120;
            CircleF[] circles = CvInvoke.HoughCircles(uimage, HoughType.Gradient, 2.0, 20.0, cannyThreshold, circleAccumulatorThreshold, 5);

            #endregion

            #region Canny and edge detection

            double cannyThresholdLinking = 120.0;
            UMat cannyEdges = new UMat();
            CvInvoke.Canny(uimage, cannyEdges, cannyThreshold, cannyThresholdLinking);

            LineSegment2D[] lines = CvInvoke.HoughLinesP(
                cannyEdges,
                1, //Distance resolution in pixel-related units
                Math.PI / 45.0, //Angle resolution measured in radians.
                20, //threshold
                30, //min Line width
                10); //gap between lines

            #endregion

            #region Find triangles and rectangles

            List<Triangle2DF> triangleList = new List<Triangle2DF>();
            List<RotatedRect> boxList = new List<RotatedRect>(); //a box is a rotated rectangle

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);
                int count = contours.Size;
                for (int i = 0; i < count; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.05, true);
                        if (CvInvoke.ContourArea(approxContour, false) > 250) //only consider contours with area greater than 250
                        {
                            if (approxContour.Size == 3) //The contour has 3 vertices, it is a triangle
                            {
                                Point[] pts = approxContour.ToArray();
                                triangleList.Add(new Triangle2DF(
                                    pts[0],
                                    pts[1],
                                    pts[2]
                                    ));
                            }
                            else if (approxContour.Size == 4) //The contour has 4 vertices.
                            {
                                #region determine if all the angles in the contour are within [80, 100] degree
                                bool isRectangle = true;
                                Point[] pts = approxContour.ToArray();
                                LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                                for (int j = 0; j < edges.Length; j++)
                                {
                                    double angle = Math.Abs(
                                        edges[(j + 1) % edges.Length].GetExteriorAngleDegree(edges[j]));
                                    if (angle < 80 || angle > 100)
                                    {
                                        isRectangle = false;
                                        break;
                                    }
                                }
                                #endregion

                                if (isRectangle) boxList.Add(CvInvoke.MinAreaRect(approxContour));
                            }
                        }
                    }
                }

                #endregion

                if (path != "def")
                {
                    original = new Bitmap(img.ToBitmap(), image2.Width, image2.Height);
                    original.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    image2.Image = original;
                }

                //Bitmap original = new Bitmap(img.ToBitmap(), image2.Width, image2.Height);
                //original.RotateFlip(RotateFlipType.RotateNoneFlipX);
                //image2.Image = original;

                #region draw triangles and rectangles

                Mat triangleRectangleImage = new Mat(img.Size, DepthType.Cv8U, 3);

                triangleRectangleImage.SetTo(new MCvScalar(0));
                foreach (Triangle2DF triangle in triangleList)
                {
                    CvInvoke.Polylines(triangleRectangleImage, Array.ConvertAll(triangle.GetVertices(), Point.Round), true, new Bgr(Color.DarkBlue).MCvScalar, 2);
                }
                foreach (RotatedRect box in boxList)
                {
                    CvInvoke.Polylines(triangleRectangleImage, Array.ConvertAll(box.GetVertices(), Point.Round), true, new Bgr(Color.DarkOrange).MCvScalar, 2);
                }

                Mat squaresImage = new Mat(img.Size, DepthType.Cv8U, 3);
                squaresImage.SetTo(new MCvScalar(0));
                foreach (RotatedRect box in boxList)
                {
                    CvInvoke.Polylines(triangleRectangleImage, Array.ConvertAll(box.GetVertices(), Point.Round), true, new Bgr(Color.DarkOrange).MCvScalar, 2);
                }

                original = new Bitmap(triangleRectangleImage.ToImage<Bgr, byte>().ToBitmap(), squareImage.Width, squareImage.Height);
                original.RotateFlip(RotateFlipType.RotateNoneFlipX);
                squareImage.Image = original;


                //Draw Squares
                if (boxList != null)
                {
                    for (int i = 0; i < boxList.Count / 2; i++)
                    {
                        if (boxList[i].Size.Width > 0)
                        {
                            addSquare(Color.Black);
                        }
                    }
                }


                //Draw Triangles
                if (triangleList != null)
                {
                    for (int i = 0; i < triangleList.Count / 2; i++)
                    {
                        if (triangleList[i].Area > 0)
                        {
                            addTriangle(Color.Black);
                        }
                    }
                }

                #endregion

                original = new Bitmap(triangleRectangleImage.ToImage<Bgr, byte>().ToBitmap(), triangleImage.Width, triangleImage.Height);
                original.RotateFlip(RotateFlipType.RotateNoneFlipX);
                triangleImage.Image = original;

                #region draw circles

                Mat circleImage = new Mat(img.Size, DepthType.Cv8U, 3);
                circleImage.SetTo(new MCvScalar(0));
                foreach (CircleF circle in circles)
                    CvInvoke.Circle(circleImage, Point.Round(circle.Center), (int)circle.Radius, new Bgr(Color.Brown).MCvScalar, 2);

                // circleImagePictureBox.Image = circleImage.ToImage<Bgr, byte>().ToBitmap();

                if (circles != null)
                {
                    foreach (CircleF circle in circles)
                    {
                        if (circle.Radius > 0)
                        {
                            addCircle(Color.Black);
                        }
                    }
                }

                #endregion

                original = new Bitmap(circleImage.ToImage<Bgr, byte>().ToBitmap(), circlePB.Width, circlePB.Height);
                original.RotateFlip(RotateFlipType.RotateNoneFlipX);
                circlePB.Image = original;

                #region draw lines

                Mat lineImage = new Mat(img.Size, DepthType.Cv8U, 3);
                lineImage.SetTo(new MCvScalar(0));
                foreach (LineSegment2D line in lines)
                    CvInvoke.Line(lineImage, line.P1, line.P2, new Bgr(Color.Green).MCvScalar, 2);


                if (path != "def")
                {
                    original = new Bitmap(lineImage.ToImage<Bgr, byte>().ToBitmap(), squareImage.Width, squareImage.Height);
                    original.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    squareImage.Image = original;
                }
                else
                {
                    original = new Bitmap(lineImage.ToImage<Bgr, byte>().ToBitmap(), image2.Width, image2.Height);
                    original.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    image2.Image = original;
                }

                #endregion
            }
        }

        private void loadImage_btn_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog
            {
                Title = "Select Image",
                CheckFileExists = true,
                CheckPathExists = true
            };

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                path = ofd.FileName;
            }

            clearBackground();

            detectShape(null);
        }

        private void ProcessImage(Mat image)
        {
            Stopwatch watch = Stopwatch.StartNew(); // time the detection process

            List<Mat> stopSignList = new List<Mat>();
            List<Rectangle> stopSignBoxList = new List<Rectangle>();
            stopSignDetector.DetectStopSign(image, stopSignList, stopSignBoxList);

            watch.Stop(); //stop the timer

            Point startPoint = new Point(10, 10);

            for (int i = 0; i < stopSignList.Count; i++)
            {
                Rectangle rect = stopSignBoxList[i];
                AddLabelAndImage(
                   ref startPoint,
                   String.Format("Stop Sign [{0},{1}]:", rect.Location.Y + rect.Width / 2, rect.Location.Y + rect.Height / 2),
                   stopSignList[i]);
                CvInvoke.Rectangle(image, rect, new Bgr(Color.Aquamarine).MCvScalar, 2);
            }

            Bitmap original = new Bitmap(image.ToImage<Bgr, byte>().ToBitmap(), image2.Width, image2.Height);
            original.RotateFlip(RotateFlipType.RotateNoneFlipX);
            squareImage.Image = original;
        }

        private void AddLabelAndImage(ref Point startPoint, String labelText, IImage image)
        {
            Label label = new Label();
            triangleImage.Controls.Add(label);
            label.Text = labelText;
            label.Width = 100;
            label.Height = 30;
            label.Location = startPoint;
            startPoint.Y += label.Height;

            ImageBox box = new ImageBox();
            triangleImage.Controls.Add(box);
            box.ClientSize = image.Size;
            box.Image = image;
            box.Location = startPoint;
            startPoint.Y += box.Height + 10;
        }

        private void clearBackground()
        {
            timer2.Stop();
        }

        private void clear_btn_Click(object sender, EventArgs e)
        {
            indexX = 0;
            indexY = 0;
            point_auxX = 0;
            point_auxY = 0;
            history.Clear();
            actual_index = -1;
            setBackground();
        }

        private void faceRecording_btn_Click(object sender, EventArgs e)
        {
            if (captureFaceProgress != false)
            {
                faceRecording_btn.Text = "Start Recording";
                captureFaceProgress = false;
                timer1.Stop();
            }
            else
            {
                faceRecording_btn.Text = "Stop Recording";
                captureFaceProgress = true;
                timer1.Start();
            }
        }

        private void shapeRecording_btn_Click(object sender, EventArgs e)
        {
            if (captureShapeProgress != false)
            {
                shapeRecording_btn.Text = "Start Recording";
                captureShapeProgress = false;
                timer2.Stop();
            }
            else
            {
                shapeRecording_btn.Text = "Stop Recording";
                captureShapeProgress = true;
                timer2.Start();
            }
        }

    }
}
