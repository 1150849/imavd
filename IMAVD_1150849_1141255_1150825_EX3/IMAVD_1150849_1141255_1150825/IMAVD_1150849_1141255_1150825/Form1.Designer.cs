﻿namespace IMAVD_1150849_1141255_1150825
{
    partial class Application
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.shapeRecording_btn = new System.Windows.Forms.Button();
            this.faceRecording_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.image2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.image1 = new System.Windows.Forms.PictureBox();
            this.squareImage = new System.Windows.Forms.PictureBox();
            this.triangleImage = new System.Windows.Forms.PictureBox();
            this.circlePB = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.loadImage_btn = new System.Windows.Forms.Button();
            this.clear_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.squareImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.triangleImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circlePB)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(453, 9);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(772, 414);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // shapeRecording_btn
            // 
            this.shapeRecording_btn.AutoSize = true;
            this.shapeRecording_btn.Location = new System.Drawing.Point(57, 765);
            this.shapeRecording_btn.Name = "shapeRecording_btn";
            this.shapeRecording_btn.Size = new System.Drawing.Size(116, 27);
            this.shapeRecording_btn.TabIndex = 13;
            this.shapeRecording_btn.Text = "Stop Recording";
            this.shapeRecording_btn.UseVisualStyleBackColor = true;
            this.shapeRecording_btn.Click += new System.EventHandler(this.shapeRecording_btn_Click);
            // 
            // faceRecording_btn
            // 
            this.faceRecording_btn.AutoSize = true;
            this.faceRecording_btn.Location = new System.Drawing.Point(158, 366);
            this.faceRecording_btn.Name = "faceRecording_btn";
            this.faceRecording_btn.Size = new System.Drawing.Size(117, 27);
            this.faceRecording_btn.TabIndex = 12;
            this.faceRecording_btn.Text = "Start Recording";
            this.faceRecording_btn.UseVisualStyleBackColor = true;
            this.faceRecording_btn.Click += new System.EventHandler(this.faceRecording_btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 406);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Shape Detection";
            // 
            // image2
            // 
            this.image2.Location = new System.Drawing.Point(12, 428);
            this.image2.Name = "image2";
            this.image2.Size = new System.Drawing.Size(430, 331);
            this.image2.TabIndex = 10;
            this.image2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Face Detection";
            // 
            // image1
            // 
            this.image1.Location = new System.Drawing.Point(12, 29);
            this.image1.Name = "image1";
            this.image1.Size = new System.Drawing.Size(430, 331);
            this.image1.TabIndex = 8;
            this.image1.TabStop = false;
            // 
            // squareImage
            // 
            this.squareImage.Location = new System.Drawing.Point(453, 488);
            this.squareImage.Name = "squareImage";
            this.squareImage.Size = new System.Drawing.Size(249, 231);
            this.squareImage.TabIndex = 14;
            this.squareImage.TabStop = false;
            // 
            // triangleImage
            // 
            this.triangleImage.Location = new System.Drawing.Point(720, 488);
            this.triangleImage.Name = "triangleImage";
            this.triangleImage.Size = new System.Drawing.Size(249, 231);
            this.triangleImage.TabIndex = 15;
            this.triangleImage.TabStop = false;
            // 
            // circlePB
            // 
            this.circlePB.Location = new System.Drawing.Point(985, 488);
            this.circlePB.Name = "circlePB";
            this.circlePB.Size = new System.Drawing.Size(249, 231);
            this.circlePB.TabIndex = 16;
            this.circlePB.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(450, 468);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Square/Lines Detection";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(717, 468);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Triangle/Square Detection";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(982, 468);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 17);
            this.label5.TabIndex = 19;
            this.label5.Text = "Circle Detection";
            // 
            // loadImage_btn
            // 
            this.loadImage_btn.AutoSize = true;
            this.loadImage_btn.Location = new System.Drawing.Point(276, 765);
            this.loadImage_btn.Name = "loadImage_btn";
            this.loadImage_btn.Size = new System.Drawing.Size(99, 27);
            this.loadImage_btn.TabIndex = 20;
            this.loadImage_btn.Text = "Select Image";
            this.loadImage_btn.UseVisualStyleBackColor = true;
            this.loadImage_btn.Click += new System.EventHandler(this.loadImage_btn_Click);
            // 
            // clear_btn
            // 
            this.clear_btn.AutoSize = true;
            this.clear_btn.Location = new System.Drawing.Point(1149, 429);
            this.clear_btn.Name = "clear_btn";
            this.clear_btn.Size = new System.Drawing.Size(75, 27);
            this.clear_btn.TabIndex = 21;
            this.clear_btn.Text = "Clear ";
            this.clear_btn.UseVisualStyleBackColor = true;
            this.clear_btn.Click += new System.EventHandler(this.clear_btn_Click);
            // 
            // Application
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1237, 800);
            this.Controls.Add(this.clear_btn);
            this.Controls.Add(this.loadImage_btn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.circlePB);
            this.Controls.Add(this.triangleImage);
            this.Controls.Add(this.squareImage);
            this.Controls.Add(this.shapeRecording_btn);
            this.Controls.Add(this.faceRecording_btn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.image2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.image1);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Application";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Application";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.squareImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.triangleImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circlePB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button shapeRecording_btn;
        private System.Windows.Forms.Button faceRecording_btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox image2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox image1;
        private System.Windows.Forms.PictureBox squareImage;
        private System.Windows.Forms.PictureBox triangleImage;
        private System.Windows.Forms.PictureBox circlePB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button loadImage_btn;
        private System.Windows.Forms.Button clear_btn;
    }
}

